# ANC Handbook

[ANC Handbook](https://handbook.anc.plus.ac.at/)

This is the source code of the ANC handbook. If you want to contribute, please see our [contribution guidelines](CONTRIBUTING.md)

The handbook is implemented with [MkDocs](https://www.mkdocs.org/) and uses the [material for mkdocs](https://squidfunk.github.io/mkdocs-material/) theme, and several supported markdown extensions. Additionally we use the following pl

We help ourselves with [GitLab Pages MkDocs example](https://gitlab.com/pages/mkdocs) to set it up.

## Build locally

1. Install mkdocs in virtual environment.
   ```
   python -m venv .venv
   source .venv/bin/activate
   pip install -U -r requirements.txt
   ```
2. Preview your project: `mkdocs serve`, your site can be accessed under `localhost:8000`
3. Add content
4. Generate the website: mkdocs build (optional)

## Create an offline version

The handbook can be also be used offline.

In order to enable offline use of the handbook you can build it yourself following the instructions under [*Build locally*](#build-locally), or download it directly from the project.

1. In the project, under Pipelines, go to Artifacts. This shows an overview of the artifacts produced by all the latest jobs. 
2. Find the latest job executed on the main branch. 
3. Unfold the 3 files that were generated
4. Download the artifacts.zip file.
5. Unzip the file

To open the handbook you can open to the home page by opening the `index.html` file located in the public directory using any browser. You can use the website as normal, including its search functionality.

To make access on an offline system easier for users, simply create a desktop shortcut to the `index.html` file. Rename it to `ANC handbook`. If you want you can use the `desktop.ico` under `docs/assets` as the desktop shortcut icon (right click > properties > change icon).
