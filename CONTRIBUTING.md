# Guidelines for working on the handbook

You can contribute to the handbook in two ways. First is to create an issue. You can create an issue if you have an idea for new content in the handbook, but also if you find a mistake, or some part of the documentation is unclear to you.

You can also contribute by solving an issue, which can be implementing a correction, or even writing new content for the ANC handbook. The handbook uses material for mkdocs. You can find more information about this in the `README.md`. Before you work on the handbook, please read these contributing guidelines to learn more about how the handbook is organized, and the style we expect on a handbook page. You also find more information in the README about how to build the handbook locally.

## Create an issue

To contribute to the ANC handbook, please create an issue using one of the provided templates. There are four types:

- New page
- New section
- Update content
- Update interface

Gitlab issue templates are provided in markdown. You can select *Rich text editing* at the bottom of the editing window to get a more regular view. The templates consist of several sections. Please fill in the requested information under the headers. The templates specify what information you should provide before an issue can be handled. Each template gives some checkboxes. If you were able to fill in the requested information, mark to box so that an issue can be picked up sooner. If you don't know something, you can leave it blank, and leave the checkmark open, the issue will be reviewed. New content issues are always reviewed before they can be handled.

## Organization of handbook

To ensure documentation in the handbook is organized consistently it is important to understand the layout of the handbook before adding new content. 

The handbook consists of sections and pages. A section is a group of pages. Sections can be nested. Each section has an index page `index.md`, which functions as a starting page for that section, with information on what the section is about, and what the current content is.

Currently there are six top level sections.

It is important that you add your documentation under the right section, to make it easy for the user to find. If you are not sure where it belongs, please ask.

**Home** contains a brief introduction to each section of the handbook. Additionally there is a page with quick links. This page contains link to information that can be quickly relevant in specific situation, such as how to deal with specific irregularities during data collection.

Under **Terms of use** you find legal documents pertaining to use of the ANC and the datasets on the ANC. This part of the handbook is carefully versioned and any changes to this part of the handbook need to be verified.

**ANC Basics** is where we describe things about the ANC and working with it. Everything that is not related to the data itself, but relates to ANC policies, tools or even GitLab, should be documented here. This section contains a mix of information pages and guides. The pages are ordered *chronologically*, or in the order we believe a user should learn about/ be able to do these things.

**Data formatting guides** lists what is required from data for it to be ANC compliant. This is of interest of people interested in depositing data to the ANC themselves, but is also of relevance for anyone working with data on the ANC. Currently, this section consist of an home page, and a page for each datatype we currently support. Each page has a BIDS section, and a further requirement section. New pages should follow the same structure.

**ANC Acquisition Guides** consist of the step by step guides for users on what to do during study planning and data acquisition to obtain ANC compatible datasets. Guides are by datatype. Per datatype each guide can consist of multiple *steps*. Each step should have it's own page. If multiple actions are given under a step these should be provided as a numbered list, but you should not use the *step* label, as this is reserved for a page. If the *step* level is not enough, because of the complexity of the process, it is also possible to add *parts*. Parts go over *steps*, so every part can have multiple steps. See the MRI acquisition guide for an example.

**ANC Tools** is currently under development and contains information about tools, software and infrastructure that is actively supported by the ANC. Here you find guides on how to work with these tools using data stored in the ANC.

## Writing documentation

### Linking between handbook pages

To link between handbook pages, use relative paths. Mkdocs converts these to proper links when rendered. There is more explanation [here](https://www.mkdocs.org/user-guide/writing-your-docs/#internal-links), but the easiest is to let vscode do all the work for you. This works even in the webIDE. To link type `./` and a dropdown menu of files and directories will appear. To go up, use `./../`. Tou can also link to specific section. Simply type `#` when you have reached the markdown file and you will see all available sections. 

These paths are validated by the pipeline to ensure the link truely exist before the handbook is published. Otherwise this is not verified. 

### Adding new page/section

New pages should be added in the right directory in the documentation. To make the page visible in the handbook, you need to add the path in the right position under `nav` in the `mkdocs.yaml` file.

If you are adding a new section, multiple pages under a new header, you should always have an index page for this section.

## Formatting tables

We support two types of formatted tables. The first is a BIDS data table. If you add a markdown table to the documentation it will be formatted as this by default. If your table is providing information, meaning, it does not contain any data, you should surround your markdown table with the following lines, so that it is formatted correctly.

``` markdown

<div class="overview_table" markdown>

|    table    | example |
| ----------- | ------- |
| information | example |

</div>

```
**Do not** add any additional formatting to your table, for example left alignment (`|:---|:---|`). This formatting is handles globally.

## Capitalization

For consistency across the pages, please take care of the following:

- Noun capitalization in headers
- Capitalize first item of a list

## Markdown conventions

We use `*` for bold or italics, and `-` for lists.
