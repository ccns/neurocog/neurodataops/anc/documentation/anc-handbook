# Sessions data

In case of multiple sessions, you can add [sessions files](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#sessions-file) to describe changing variables between sessions, for example repeated measures taken such as blood pressure. There should be one `sessions.tsv` file for each subject, containing the data, accompanied by a single `sessions.json` file at the root of the dataset.

## Format Summary

The `session.tsv` is stored within every subject directory with the prefix `sub-<label>`. The accompaning `session.json` can be stored at the root of your repository according to the [inheritance principle](https://bids-specification.readthedocs.io/en/stable/common-principles.html#the-inheritance-principle).

```txt
└── your_study/
    ├── README.md
    ├── dataset_description.json
    ├── participants.json
    ├── participants.tsv
    ├── sessions.json
    └── sub-s001/
        ├── ses-1/
        ├── ses-2/
        ├── ses-3/
        └──sub-s001_sessions.tsv
```

## Content of `sessions.tsv`

The first column of the tabular file must be `session_id` containing the session labels in the format of `session-<label>`. For each session exactly one row must exist.

| session_id | column1 | column2 |
| :---: | :----: | :---: |
| ses-label | value | value |
| ses-label | value | value |
| ses-label | value | value |

Example:

| session_id | acq_time | blood_pressure |
| :---: | :----: | :---: |
| ses-1 | 2023-03-14T16:13:26.772500 | 120 |
| ses-2 | 2023-03-21T16:02:59.492500 | 100 |
| ses-3 | 2023-03-28T16:02:18.385000 | 110 |

## Content of `sessions.json`

Every column must be described within the `sessions.json` file.

```json
    {
        "session_id": {
            "Description": "A participant ID",
            "Levels": {
                "ses-1": "description",
                "ses-2": "description",
                "ses-3": "description"
            }
        },
        "column1": {
            "Description": "description",
            "Unit": "description"
        },
        "column2": {
            "Description": "description"
        }
    }
```


