# MEG data

!!! Info "Under construction"

    These pages are currently under construction.

Don’t hesitate to convert your MEG-data! Its surprisingly easy! If you have any questions, we’re happy to help. All datasets need to contain general metadata, [participant data](../data-acquisition-guides/salzburg/questionnaire-data/step1_tsv.md). If participants performed a task experiment during MEG data collection (or were presented with any type of stimuli), it is also required to add event data. You can find some general information on how to [plan](../data-acquisition-guides/salzburg/task-experiment-data/step1_plan_events.md) your events and are some [example](task_experiment_data.md)  events.

## Format summary

MEG data should be formatted according to BIDS. There are currently no further MEG specific additional requirements, but the following general ANC requirements apply:

- Event files for non-rest time course data is are required and are treated as an error in BIDS validation
- All data must be organized in a session directory, event if there is only one session

The full BIDS specification on MEG data can be found [here](https://bids-specification.readthedocs.io/en/stable/modality-specific-files/magnetoencephalography.html).

Take note of the following BIDS terminology: A session is a collection of data recordings, usually a single appointment during which a participant came to the scanner. A run is a single continuous data recording. In order to get a valid BIDS-dataset, there should be an `meg` directory for each participant, containing one `_meg.fif` file per run. For each `.fif` file there should be an accompanying `_meg.json`, `_channels.tsv` file, and an `_events.tsv` file if your task is not `rest` (for more information on the events file and necessary metadata see [task experiment data formatting requirements](./task_experiment_data.md)).

The `_meg.fif` file contains the raw data, the `_meg.json` file includes information about the MEG system, such as sampling rate etc., the `channels.tsv` file includes information about the channels and the [`events.tsv`](./task_experiment_data.md) file describes the tasks or stimuli and their timing.

The data should be structured as follows:

```txt
└── ns_lipreading/
    ├── README.md
    ├── dataset_description.json
    ├── participants.json
    ├── participants.tsv
    ├── task-lipreading_events.json
    └── sub-1d49a7b4b040/
        └── ses-01/
            ├── sub-1d49a7b4b040_ses-01_scans.tsv
            └── meg/
                ├── sub-1d49a7b4b040_ses-01_coordsystem.json
                ├── sub-1d49a7b4b040_ses-01_task-lipreading_run-01_channels.tsv
                ├── sub-1d49a7b4b040_ses-01_task-lipreading_run-01_events.tsv
                ├── sub-1d49a7b4b040_ses-01_task-lipreading_run-01_meg.fif
                ├── sub-1d49a7b4b040_ses-01_task-lipreading_run-01_meg.json
                ├── sub-1d49a7b4b040_ses-01_task-lipreading_run-02_channels.tsv
                ├── sub-1d49a7b4b040_ses-01_task-lipreading_run-02_events.tsv
                ├── sub-1d49a7b4b040_ses-01_task-lipreading_run-02_meg.fif
                └── sub-1d49a7b4b040_ses-01_task-lipreading_run-02_meg.json
```

In addition to the actual data, please also include the time-wise closest **empty room measurement**. 

```txt
└── sub-emptyroom/
    └── ses-20190619/
        ├── sub-emptyroom_ses-20190619_scans.tsv
        └── meg/
            ├── sub-emptyroom_ses-20190619_task-noise_channels.tsv
            ├── sub-emptyroom_ses-20190619_task-noise_meg.fif
            └── sub-emptyroom_ses-20190619_task-noise_meg.json
```

## Prerequisites

To successfully convert your MEG data you need at least the following.

- Access to the .fiff files of the MEG data (including empty room files if applicable)
- Translation of your trigger/photodiode/.log-files/.mat-files that indicates when each stimulus was presented
- Information about the different conditions and stimuli

## Tools

Are you using Fieldtrip or MNE? Both have built-in BIDS functions that can help you transform and save your data. Check it out: [Fieldtrip](https://www.fieldtriptoolbox.org/example/bids_meg/)/ [MNE](https://mne.tools/mne-bids/stable/use.html)
