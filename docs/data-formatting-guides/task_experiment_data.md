# Task experiment data

!!! Info "Under construction"

    These pages are currently under construction.

## Format summary

With task experiment we mean one of two BIDS data types. Purely behavioral task experiments should be stored as [BIDS behavioral data](https://bids-specification.readthedocs.io/en/stable/modality-specific-files/behavioral-experiments.html) with a `beh` subdirectory in the subject directories. Task experiments that were performed while recording time course data such as functional MRI or MEG data should be stored as [events files](https://bids-specification.readthedocs.io/en/stable/modality-specific-files/task-events.html) along with the time course data. The organization of files as well as content is very similar between these cases, except that `_events.tsv` files should have at least an *onset* and *duration* column. The onset column should contain the onset of the described event relative to the onset of the collected time course data to support analysis. In the following we refer to both behavioral data files and events data files as events files.

### `events.tsv` files

Task data is stored in tabular files where every line represents an experimental event.

The columns and values are mainly decided by the data collector and depend on the kind of task experiment that was performed, but can contain information on what type event it is (e.g. a response or stimulus presentation), or the color of specific stimulus. Empty values in are normal (a response event does not have a value stimulus color) and should be marked with an `n/a` string in accordance with BIDS. We provide a [step by step guide](../data-acquisition-guides/salzburg/task-experiment-data/step1_plan_events.md) on how to design an event file. It is designed to be followed before an experiment, but you can use it determine how to convert you experiment log files into events files (also see [tools](#tools)).

### `events.json` files

Along such a tabular file there should be a sidecar `.json` file, which should describe the columns and values in the tabular file. Although BIDS inheritance principle allows these files to exist at any level in the dataset, we strongly recommend create one `json` file per task, which should be stored at the top level of the dataset. A general requirement of the ANC is that all tabular data has to be described in a json sidecar.  For each column-value pair, there should be at least a free text description. For this we distinguish between categorical or value columns, which should be described as follows:

```json

{
    "categorical_column": {
        "Description": "Describe the content of the column here",
        "Levels": {
            "A": "Describe what this column value means",
            "B": "Describe what this column value means"
        }
    },
    "value_column": {
        "Description": "Describe the content of the column here",
        "Unit": "(Optional) Describe the unit of the value when relevant"
    }
}

```

Additionally, events data can be annotated with [Hierarchical Event Descriptors](https://www.hedtags.org/), providing a machine actionable description of your events. 

There should be one HED tag for each categorical column value and a single tag to describe a value column. This tag should contain a `#`, which represents the individual column values.

```json

{
    "categorical_column": {
        "Description": "Describe the content of the column here",
        "Levels": {
            "A": "Describe what this column value means",
            "B": "Describe what this column value means"
        },
        "HED": {
            "A": "(Sensory-event, Visual-presentation)",
            "B": "(Sensory-event, Auditory-presentation)"
        }
    },
    "value_column": {
        "Description": "Describe the content of the column here",
        "Unit": "(Optional) Describe the unit of the value when relevant",
        "HED": "(Sensory-event, Frequency/# Hz)"
    }
}

```

For more information about HED tags, see the [HED documentation](https://www.hed-resources.org/en/develop/BidsAnnotationQuickstart.html). You can also got to our [acquisition guide]() for step-by-step tutorial on generating a `.json` file for your events file and adding HED annotations.


### Examples

Example of an MEG experiment events file, with a column for `sample`, `event_type`, `value`, and `lip_file` and `speech_file`, providing a path to other files used for analysis.

=== "Continuous speech `.tsv` file"

    |   onset |   duration |   sample | event_type   |   value | lip_file                                   | speech_file                                      |
    |---------|------------|----------|--------------|---------|--------------------------------------------|--------------------------------------------------|
    |   3.994 |    44.736  |     3994 | forward      |      33 | liparea_33.tsv | speech_env_33.tsv |
    |  50.606 |    43.6053 |    50606 | backward     |      16 | liparea_16.tsv | speech_env_16.tsv |
    |  96.585 |    49.7707 |    96585 | backward     |      23 | liparea_23.tsv | speech_env_23.tsv |
    | 147.784 |    45.76   |   147784 | forward      |      41 | liparea_41.tsv | speech_env_41.tsv |

=== "Continuous speech `.json` file"

    ```json
        {
            "event_type": {
                "Description": "Description for event_type",
                "HED": {
                "backward": "(Sensory-event, Visual-presentation, (Video-clip, (Human-agent),((Speak, Backward))))",
                "forward": "(Sensory-event, Visual-presentation, (Video-clip, (Human-agent),((Speak, Forward))))"
                },
                "Levels": {
                "backward": "videos presented in a reversed manner. Trigger values between [77, 88] or [13, 24]",
                "forward": "videos presented in a forward manner. Trigger values between [97, 108] or [33, 44]"
                }
            },
            "duration": {
                "Description": "duration of the stimulus in seconds (predefined by the stimulus--> convert into samplingpoints)"
            },
            "sample": {
                "Description": "Data sample in which this event started"
            },
            "value": {
                "Description": "Trigger value associated with a specific video"
            },
            "lip_file": {
                "Description": "Filename of the lip-area extracted with the chimera toolbox"
            },
            "speech_file": {
                "Description": "Filename of the speech envelope"
            }
        }
    ```

Example of an fMRI experiment presenting words, with columns `trial_type`, `word` and many columns describing word metadata.

=== "Repeat words `.tsv` file"

| onset  | duration | trial_type    | word | concreteness | log_freq | den_b | freq_den_intern  | pos  | tense   | log_10_wf |
| ------ | -------- | ------------- | ---- | ------------ | -------- | ----- | ---------------- | ---- | ------- | --------- |
| 0      | 0        | RepeatCorrect | rut  | 3.64         | 6.05     | 28    | 169.4            | noun | n/a     | 1.8976    |
| 6.139  | 0        | RepeatCorrect | hawk | 4.93         | 8.19     | 18    | 147.42           | noun | n/a     | n/a       |
| 12.279 | 0        | RepeatCorrect | rip  | 3.79         | 8.99     | 32    | 287.68           | verb | present | 3.0124    |
| 18.418 | 0        | RepeatNoise   | n/a  | n/a          | n/a      | n/a   | n/a              | n/a  | n/a     | n/a       |
| 36.836 | 0        | RepeatCorrect | wick | 4.69         | 6.33     | 26    | 164.58           | noun | n/a     | n/a       |
| 42.975 | 0        | RepeatCorrect | tug  | 3.5          | 7.07     | 18    | 127.26           | verb | present | 2.1492    |

=== "Repeat words `.json` file"

    ```json
        {
            "trial_type": {
                "Description": "Description for trial_type",
                "HED": {
                    "RepeatNoise": "(Sensory-event,Auditory-presentation,(Sound, Not-meaningful)), ((Agent-action,Human-agent,Experiment-participant), (Repeat, Sound),(Fixate,Cross))",
                    "RepeatIncorrect": "(Sensory-event,Auditory-presentation,(Word, {pos}, {tense}, {word}, {log_10_wf}, (English, Analytic-language-type, Deep-orthographical-type))),((Agent-action,Human-agent,Experiment-participant, Incorrect-action), (Repeat, Sound),(Fixate,Cross))",
                    "RepeatCorrect": "(Sensory-event,Auditory-presentation,(Word, {pos}, {tense}, {word}, {log_10_wf}, (English, Analytic-language-type, Deep-orthographical-type))),((Agent-action,Human-agent,Experiment-participant, Correct-action), (Repeat, Sound),(Fixate,Cross))"
                },
                "Levels": {
                    "RepeatCorrect": "Participant was presented with an auditory word and correctly repeated this word while looking at a fixation cross",
                    "RepeatIncorrect": "Participant was presented with an auditory word but did not correctly repeat this word while looking at a fixation cross",
                    "RepeatNoise": "Participant was presented with noise and asked to repeat but no correct response possible"
                }
            },
            "pos": {
                "Description": "Part of speech with which a word appears in American English most frequently based on data from Corpus of Contemporary American English",
                "HED": {
                    "adjective": "Adjective",
                    "noun": "Noun",
                    "verb": "Verb"
                },
                "Levels": {
                    "adjective": "Adjective",
                    "noun": "Noun",
                    "verb": "Verb"
                }
            },
            "tense": {
                "Description": "The tense of a presented verb",
                "HED": {
                    "past": "Past-tense",
                    "present": "Present-tense"
                },
                "Levels": {
                    "past": "Verb in simple past tense",
                    "present": "Verb in present tense"
                }
            },
            "word": {
                "Description": "The presented word",
                "HED": "(ID/#)"
            },
            "concreteness": {
                "Description": "A measure of word concreteness based on"
            },
            "log_freq": {
                "Description": "A measure of word frequency as measured by the log of the Hyperspace Analogue to Language dataset"
            },
            "den_b": {
                "Description": "Likely a measure of phonological density"
            },
            "freq_den_intern": {
                "Description": "Likely a measure related to phonological density"
            },
            "log_10_wf": {
                "Description": "Log frequency of word occurance based on the SUBTLEXus dataset: https://www.ugent.be/pp/experimentele-psychologie/en/research/documents/subtlexus",
                "HED": "(Word-frequency, Parameter-value/#, (Computed-feature, Label/SUBTLEXus))"
            }
        }
    ```

## Tools

* [Generate `json` file and validate HED tags](https://hedtools.org/hed/)
* [Convert NeuroObs presentation log files to events](https://gitlab.com/ccns/neurocog/neurodataops/anc/software/presentation-to-bids)
* [Remap and adjust events files to make them more complete or descriptive using the Remodeler](https://www.hed-resources.org/en/develop/HedRemodelingTools.html)