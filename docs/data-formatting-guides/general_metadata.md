# General metadata

Every dataset must contain the following three files containing general metadata: 

- `README.md`, 
- `dataset_description.json` 
- `CITATION.cff`

Each of these files are part of the [BIDS specification](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html), but note that the `CITATION.cff` is OPTIONAL under BIDS, while we require it to be present and filled in. Instructions on how to fill in these files and which fields are required is provided in the [ANC template issues](./../anc-basics/navigate_dataset_project.md#template-issues).

## Format summary

```txt
└── your_repository/
    ├── README.md
    ├── dataset_description.json
    ├── CITATION.cff
    ├── participants.json
    ├── participants.tsv
    └── sub-01/
        └── ses-01/

```
 
- `README.md`: a general description of your dataset, like the general idea, methodes, and tasks. This description should help others to understand your study and data. Additionally, information about missing files or issues in the data should be provided. This readme is also provided publicly on the dataset website.

- `dataset_description.json`: dataset metadata that includes information about funding and ethical approval, information about BIDS version is maintained by the data stewards of the ANC.

- `CITATION.cff`: contains information about how this dataset should be cited, your dataset website is generated based on this file. We also rely on this file for generating DOIs
