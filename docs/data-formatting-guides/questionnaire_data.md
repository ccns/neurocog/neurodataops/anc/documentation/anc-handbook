# Questionnaire data

This is an outline of the requirements for questionnaire data that comes with your neuroimaging data.In the [BIDS specification](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#phenotypic-and-assessment-data) this type of data is referred to as phenotype data. The representation of phenotypic data within the ANC must conform to the BIDS specification.

## Format summary

The current [BIDS specification](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#phenotypic-and-assessment-data) proposes phenotpypic data is stored in a top level directory `phenotype`. Data is stored in tabular files, along with accompanying `.json` files.

```txt
└── phenotype/
    ├── <measurement_tool-name>.tsv
    └── <measurement_tool-name>.json
```

To pass the validation the `<measurement_tool_name>.tsv` MUST have a column `participant_id` which contents MUST correspond to a `sub-` directory in the dataset and the `participant.tsv`.

### Content of `<measurement_tool-name.tsv`

| participant_id | item_1 | item_2 | item_3 |
|---|---|---|---|
| sub-01 | 1 | 2 | 1 |
| sub-02 | 3 | 1 | 2 |

## Further requirements

The BIDS specification does not currently cover several common use cases:

* storing pre-screening data / storing varying questionnaires across subjects
* storing drop outs
* storing longitudinal data meaningfully

The following table describes how to represent these use cases in the ANC.

<div class="overview_table" markdown>

| Issue | Error cause | Solution |
|-------|-------------|-----------------------|
| Pre-screening data | The bids-validator errors out if a `participant_id`of the `<measurement-tool>.tsv`file does not correspond to a `sub-` directory. | Store the phenotypic data for participants that do not have imaging data in a subdirectory (e.g. `pheno-extra`) in the `/phenotype` dir and add this to the `.bidsignore`. |
| Varying questionaires across subjects / Storing drop-outs | The bids-validator errors out if a `participant_id`of the `<measurement-tool.tsv`file does not correspond to a `sub-` directory. Here, theoretically each `participant_id` corresponds to a `sub-` directory, but not every `<measurement_tool>.tsv` contains every `participant_id` | Create the `participant_id` in every `<measurement_tool>.tsv` and fill it with `n/a`. |
| Storing longitudinal data meaningfully | The current specification does not take repeated phenotypic measures into account | Add the `ses-` label to the measurement tool `.tsv` name (e.g., `<measurement-tool-ses-1>.tsv`, `<measurement-tool-ses-2>.tsv`). |

</div>

### Example 

The following shows an example of the directory structure in case of participant drop-out.

```txt
├── sub-01/
│   ├── ses-01/
│   │   ├── anat/
│   │   └── func/
│   └── ses-02/
│       ├── anat/
│       └── func/
├── sub-02/
│   └── ses-01/
│       ├── anat/
│       └── func/
└── phenotype/
    ├── <measurement_tool-ses-01>.tsv
    ├── <measurement_tool-ses-01>.json
    ├── <measurement_tool-ses-02>.tsv
    └── <measurement_tool-ses-02>.json
```

The tabular file for the second session would look as follows:

| participant_id | item_1 | item_2 | item_3 |
|---|---|---|---|
| sub-01 | 1 | 2 | 1 |
| sub-02 | n/a | n/a | n/a |
