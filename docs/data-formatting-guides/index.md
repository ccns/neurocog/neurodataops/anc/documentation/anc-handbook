# Data formatting guides

If you are interested in adding your data to the ANC, the data should be formatted according to [BIDS](https://bids-specification.readthedocs.io/en/stable/) and satisfy several additional requirements. The formal requirements can be found in our Terms of Use. The following section provides a guide to help you convert existing data into the right format. If you are planning a data acquisition, please check our data acquisition guides as it is usually easier to produce BIDS valid data from the start than it is to convert.

The exact requirements for your data are dependent on the datatype. We currently actively provide guides on conversion of MEG, MRI and questionnaire data.  

Independent of which data types you have collected, there are also general requirements that apply to each dataset. You should always [open a dataset project from template](../anc-basics/create_dataset_project.md) and resolve the template issues that are provided to ensure the basic dataset metadata is covered.
