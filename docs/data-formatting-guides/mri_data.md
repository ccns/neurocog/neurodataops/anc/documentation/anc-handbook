# MRI data

!!! Info "Under construction"

    These pages are currently under construction.

## Format summary

MRI data should be formatted according to BIDS. The full BIDS specification on MRI data can be found [here](https://bids-specification.readthedocs.io/en/stable/modality-specific-files/magnetic-resonance-imaging-data.html).

Within the ANC, all data must be organized in a session directory, event if there is only one session. Take note of the following BIDS terminology: A session is a collection of data recordings, usually a single appointment during which a participant came to the scanner.

The MRI sequences are stored in `.nii` format accompanied by a corresponding `.json` file containing the metadata usually retrieved from the dicom header. These files are organisied within the session directory according to their data types. A `scans.tsv` file listing all acquired sequences must be added per session.

One crucial point is the file naming, as every sequence type has its very specific suffix, e.g. Blood-Oxygen-Level Dependent image is labeled `_bold` or a T1-weighted image like a MP-RAGE sequence is labeld `T1w`. Depending on the sequence type additional labels might be needed, for example the phase encoding direction of diffusion weighted images. For more information on the sequence naming see our [guide on preparing MRI sequences](./../data-acquisition-guides/salzburg/mri-data/part1/step2_sequence_naming.md). Also the required metadata within the `.json` file varies between sequence types. Tools for converting your raw data into BIDS format, for example [BIDScoin](https://bidscoin.readthedocs.io/en/stable/), usually take care of this. Otherwise, please review the [BIDS Documenation](https://bids-specification.readthedocs.io/en/stable/modality-specific-files/magnetic-resonance-imaging-data.html).

The data should be structured as follows:

```txt
└── MRI_Dataset/
    ├── README.md
    ├── dataset_description.json
    ├── participants.json
    ├── participants.tsv
    ├── task-nback_events.json
    └── sub-001/
        └── ses-01/
            ├── sub-001_ses-01_scans.tsv
            ├── anat
                ├── sub-001_ses-01_T1w.json
                └── sub-001_ses-01_T1w.nii
            ├── dwi
                ├── sub-001_ses-01_dir-PA_dwi.bval
                ├── sub-001_ses-01_dir-PA_dwi.bvec
                ├── sub-001_ses-01_dir-PA_dwi.json
                └── sub-001_ses-01_dir-PA_dwi.nii
            ├── fmap
                ├── sub-001_ses-01_phasediff.json
                └── sub-001_ses-01_phasediff.nii
            └── func
                ├── sub-001_ses-01_task-rest_bold.json
                ├── sub-001_ses-01_task-rest_bold.nii
                ├── sub-001_ses-01_task-nback_bold.json
                ├── sub-001_ses-01_task-nback_bold.nii
                └── sub-001_ses-01_task-nback_bold_events.tsv
```

## Associated Data

If task data other then resting state was acquired during the scanning sequences, event files must be added. Thus see [task experiment data](./task_experiment_data.md).

It task data independent of the scanning sequences was acquired, also see [task experiment data](./task_experiment_data.md). This data should be added within the [behavioral data directory `beh`](https://bids-specification.readthedocs.io/en/stable/modality-specific-files/behavioral-experiments.html).

If multiple sessions have been acquired, a [`sessions.tsv` file](./sessions_data.md) should be added.
