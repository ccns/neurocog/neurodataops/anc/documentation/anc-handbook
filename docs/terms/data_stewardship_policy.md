# Data Stewardship Policy

!!! info "History"
   
    :material-tag: **1.0.0** Previous versions can be found in the [Agreement history](./index.md#agreement-history).

The Austrian NeuroCloud (ANC) repository is committed to maintaining the highest standards of data management and quality. To achieve this, the repository employs dedicated **Data Stewards** who actively oversee the lifecycle of all submitted datasets. Their role is crucial in ensuring that all datasets adhere to the latest version of the [Brain Imaging Data Structure (BIDS) specification](https://bids-specification.readthedocs.io/en/stable/) and that they meet the repository’s stringent data quality requirements.

Data Stewards play a key role in mitigating the burden on depositors, who might otherwise need to handle complex tasks such as learning the BIDS specification, developing [FAIR](https://www.go-fair.org/fair-principles/) (Findable, Accessible, Interoperable, Reusable) dissemination strategies, acquiring the technical skills to implement these strategies, and staying up to date with the newest developments in BIDS and FAIR. By working closely with the Data Stewards, depositors can focus on their research while ensuring that their datasets meet high standards for sharing and reuse.

Data Stewards are responsible for:

- **Ensuring BIDS Compliance**: They review and validate each dataset to confirm compliance with the latest BIDS specification, addressing any deviations, inconsistencies or necessary updates.

- **Improving Data Format Quality**: Data Stewards will support depositors by cleaning and organizing datasets, ensuring consistency across files, validating formats, checking for errors or missing data, and guiding the implementation of best practices.

- **Issue Resolution Support**: Data Stewards assist depositors in resolving technical or formatting issues related to their datasets, providing guidance on adhering to BIDS specification and repository-specific requirements.

- **Metadata Oversight**: They monitor the accuracy and dissemination of metadata to ensure that all datasets are accompanied by comprehensive and accurate descriptions, which are essential for long-term usability and discoverability.

- **Pre-DOI Quality Checks**: Before Digital Object Identifiers (DOIs) are assigned, Data Stewards thoroughly check the quality of datasets, including completeness, consistency, and compliance with both BIDS specification and [Data Format Requirements](./data_format_requirements.md).

Data depositors are expected to **actively collaborate** with the Data Stewards throughout the curation process to quickly address any issues and enhance the quality of their datasets. This cooperative approach ensures that datasets are of the highest standard and ready for dissemination in a timely manner.

In addition to their curation responsibilities, Data Stewards also represent the Austrian NeuroCloud in various collaborative efforts relevant to the repository, such as the development of the BIDS specification and software tools used by the repository. Through these efforts, Data Stewards contribute to the ongoing improvement of neuroimaging data standards and infrastructure, further supporting the research community.
