# Public Data Sharing Policy

!!! info "History"
   
    :material-tag: **1.0.0** Previous versions can be found in the [Agreement history](./index.md#agreement-history).

The Austrian NeuroCloud (ANC) repository is committed to fostering data sharing and collaboration within the research community. However, the **Public Data Sharing Policy** outlines the guidelines and restrictions for when datasets can be shared with the general public.

## Sharing conditions

By default, all datasets uploaded to the ANC are private and accessible only to the depositor. Datasets can be shared with other ANC users under the terms of the [ANC License](./anc_license.md), as specified in [Transfer and License Agreement](./transfer_and_license_agreement.md). Sharing datasets with the general public, however, is subject to stricter regulations.

The following conditions exclude datasets from being shared with the general public:

- **Sensitive or personal information** of the subjects, including but not limited to responses in questionnaires, information within the `./participants.tsv` file, and metadata contained in measurement files.
- **MRI images** that could be used to reconstruct the subject's face, including but not limited to both non-parametric and parametric structural images, BOLD images, and cerebral blood volume images. Defacing techniques, commonly applied to mitigate this risk, may reduce the potential for re-identification but can affect the results of certain analyses. Therefore, the ANC's recommendation is to deposit non-defaced original images and share the datasets with ANC users only.

For the definition of personal data, please refer to [Article 4(1) of the GDPR](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e1489-1-1). For further explanation see [the answer from the European Commission](https://commission.europa.eu/law/law-topic/data-protection/reform/what-personal-data_en).

## Metadata sharing

Metadata, as defined in the [Metadata Specification](./metadata_specification.md) and further outlined in the [Transfer and License Agreement](./transfer_and_license_agreement.md), will be made publicly available through various channels to enhance the findability of datasets. This ensures that datasets, even if not publicly shared, are discoverable by the research community.

## Verification process

Before any dataset can be made publicly available, it must undergo verification and receive explicit approval from the ANC. Data depositors are prohibited from sharing their datasets with the general public until this verification process is completed and the dataset is formally accepted for public dissemination.

Designing a transparent process is a work in progress. As a temporary solution, send your request with the name of your dataset to `anc@plus.ac.at`.
