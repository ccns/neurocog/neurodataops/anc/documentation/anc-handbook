# ANC Terms of Use

!!! info "Alignment with the requirements of the Austrian and EU data protection laws"

    In accordance with § 2d (1) Z5 lit a of the Research Organisation Act (Forschungsorganisationsgesetz - FOG), [PLUS](https://www.plus.ac.at/) informs that data processing within the scope of the [Austrian NeuroCloud](https://anc.plus.ac.at/) is carried out for research purposes on the legal basis of § 2d (2) FOG. PLUS complies with the measures provided for in Section 2d (1) FOG, including minimizing data use, protecting privacy, and ensuring data security. For more information see [Forschungsorganisationsgesetz - Bundesrecht konsolidiert](https://www.ris.bka.gv.at/GeltendeFassung.wxe?Abfrage=Bundesnormen&Gesetzesnummer=10009514).

The following Terms of Use are legally binding for all users of the Austrian NeuroCloud data repository. These documents outline the responsibilities, rights, and obligations associated with accessing, contributing to, or managing data within the repository. To ensure clarity and ease of reference, the terms are organized into [main](#main-terms) and [additional](#additional-terms) categories, aligned with the type of user activity within the repository. Each document is available on a separate webpage, accompanied by a user-friendly summary for quick reference.

**Please read the relevant documents carefully to ensure compliance with the applicable policies.**

The documents are version controlled using [Semantic Versioning for Documents](https://semverdoc.org/). In order to ensure transparency, previous versions of the terms are listed in the [Agreement history](#agreement-history).

## Main terms

<div class="overview_table" markdown>

| Type of user activity | Applicable agreement | Latest version |
| --------------------- | -------------------- | -------------- |
| General use of ANC resources | [Terms of Service](./terms_of_service.md) | 1.0.0 |
| Data deposition | [Transfer and License Agreement](./transfer_and_license_agreement.md) | 1.0.0 |
| General data sharing | [ANC License](./anc_license.md) | 1.0.0 |

</div>

## Additional terms

<div class="overview_table" markdown>

| Type of user activity | Applicable agreement | Latest version |
| --------------------- | -------------------- | -------------- |
| Dissemination of metadata | [Metadata Specification](./metadata_specification.md) | 1.0.0 |
| Data formatting | [Data Format Requirements](./data_format_requirements.md) | 1.0.0 |
| Assisted data curation | [Data Stewardship Policy](./data_stewardship_policy.md) | 1.0.0 |
| Public data sharing | [Public Data Sharing Policy](./public_data_sharing_policy.md) | 1.0.0 |
| Assigning DOIs | [Persistent Identifiers Policy](./pid_policy.md) | 1.0.0 |
| Interacting with ANC features | [Code of Conduct](./code_of_conduct.md) | 1.0.0 |

</div>

## Agreement history

Previous versions of the documents will be available here.
