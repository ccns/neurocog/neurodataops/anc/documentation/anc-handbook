# Persistent Identifiers Policy

!!! info "History"
   
    :material-tag: **1.0.0** Previous versions can be found in the [Agreement history](./index.md#agreement-history).

Persistent identifiers (PIDs) play a crucial role in ensuring the long-term accessibility and citation of datasets. By providing stable, unchanging references, PIDs make it easier for researchers to find, share, and reuse data. In the Austrian NeuroCloud (ANC), we assign two types of persistent identifiers to each dataset: a URL pointing to the dataset itself, and a pair of URL and DOI linking to a representative dataset website that adheres to the FAIR principles.

## Dataset project URL

Each dataset project in the ANC has a URL, for example:

[https://data.anc.plus.ac.at/bids-datasets/neurocog/soccer](https://data.anc.plus.ac.at/bids-datasets/neurocog/soccer)

This URL is reachable by all ANC users who were granted the access to the dataset project (restricted access), or everyone when the project is publicly available (open access). This URL allows to view the dataset and contribute to it.

This URL may change in the case of renaming the dataset project or moving it under a different group.

## Dataset website URL

The dataset website is a representative summary of a dataset metadata. It is compliant with FAIR principles and provides rich, accessible metadata and other information related to the dataset. The dataset website is publicly available to everyone.

The dataset website is automatically generated if all necessary metadata is correctly provided in the dataset and has a default URL, for example:

[https://bids-datasets.data-pages.anc.plus.ac.at/neurocog/soccer/](https://bids-datasets.data-pages.anc.plus.ac.at/neurocog/soccer/)

This URL may change in the case of renaming the dataset project or moving it under a different group.

## Dataset DOI

To enhance discoverability and citation of datasets, a persistent Digital Object Identifier (DOI) can be assigned to a dataset. The DOI links to the dataset website, as it is publicly available. Linking to datasets themselves would not work for datasets with restricted access.

The DOI ensures that the dataset remains easily referenceable and accessible over time, even if the location or the name of the dataset changes.

Given that DOIs are permanent, it is essential that they are assigned only to datasets that meet the highest standards of quality. A DOI will be generated and assigned when the following conditions are satisfied:

- **Complete Metadata**: All required metadata fields must be filled out in accordance with ANC [Data Format Requirements](./data_format_requirements.md).
- **Correct Data Format**: The dataset must conform to ANC [Data Format Requirements](./data_format_requirements.md).
- **Validation**: The dataset must pass the BIDS validation and any other automated validation provided by the ANC. In the case the validation errors cannot be resolved, exceptions will be evaluated by the Data Stewards on a case by case basis.
- **Dataset Website**: The dataset's website must automatically generate without errors. The information on the website must be correct and complete.

By ensuring these criteria are met, we maintain trust in the datasets hosted by ANC and foster a reliable data-sharing environment.
