# ANC License

!!! info "History"
   
    :material-tag: **1.0.0** Previous versions can be found in the [Agreement history](./index.md#agreement-history).

!!! abstract "What is the ANC License?"

    The ANC License is a contract between the following two parties:
    
    - **Licensor**: The person who owns or controls the rights to the dataset and allows others to use it.
    - **Licensee**: The party that is granted permission by the Licensor to use the dataset under the terms specified in this license.
    
    <h4>Overview</h4>

    - The Licensor grants permission to the Licensee to use, display, reference, and modify the dataset for scientific or educational purposes.
    - By using the dataset, the Licensee automatically agrees to the terms of the ANC License.

    <h4>Rights and obligations</h4>

    - The dataset must be cited properly by the Licensee in any related publications and in their work.
    - The Licensee must not redistribute, republish, or share the dataset with any other parties. This includes other repositories (both internal institutional repositories and global services such as OpenNeuro) and archiving services (such as Zenodo or OSF).
    - The Licensee must thoroughly document any modifications made to the dataset.
    - Other important rights, such as data protection and moral rights, remain unaffected by this License.
    - The Licensor will not charge the Licensee a fee for using the dataset.

    <h4>Warranties and liabilities</h4>

    - The Licensor does not guarantee the accuracy or quality of the datasets and is not responsible for any issues that arise from using it.

    <h4>Non-Compliance</h4>

    - Should the Licensee violate any of the licensing terms, the Licensor or the ANC can revoke access to the dataset, suspend usage, and potentially take legal action.

## 1. Preamble

1.1. By using the dataset to which this License is attached and that has been made available on the Austrian NeuroCloud (ANC), you agree to the License conditions detailed below and give assurance that you will use the dataset exclusively in accordance with the License provisions below.

1.2. In the context of this agreement, the term "dataset" shall refer to immaterial goods that are available in digital format and are, as such, tradable irrespective of a carrier, particularly via the internet. The Term "Licensor" shall refer to the entity or individual who owns or controls the rights to the dataset and grants permission for its use. The term "Licensee" shall refer to the entity or individual that receives permission from the Licensor to use the dataset under the terms specified below.

## 2. Licensing

2.1. Under the terms of this license, the Licensor hereby grants a non-sublicensable, non-exclusive, unilateral revocable license, which is unlimited in terms of time and place and free of charge to use, display, reference and edit the dataset, in full or in part, at their own discretion, and particularly to combine it with other datasets and metadata and carry out modifications.

2.2. However, the Licensee must not redistribute, republish, or transmit the licensed dataset to a third party, in whole or in part, in any form or by any means.

2.3. The licensed dataset may be used, in accordance with this License, in all known media and formats now known or created in the future, and the necessary technological modifications for this purpose may be carried out.

2.4. Once the said dataset is made available, the Licensee automatically receives an offer from the Licensor to use the dataset under the terms and conditions specified in this License. By using the said dataset, the Licensee has accepted the Licensor's offer.

2.5. The licensed dataset shall be used by the Licensee exclusively for specific scientific or educational purposes in accordance with this License. Its use for any other purpose shall not be permitted.

2.6. The Licensee is required to cite the dataset in any research results they publish, regardless of the format, if the dataset has been used in their research. The same applies if the dataset is used for educational purposes. The dataset must be cited according to the format specified by the ANC. In the bibliography or list of reference material, the following information must be included in full:

- The full names of the Author(s) and/or organizations, which produced the Dataset
- The title of the Dataset
- The DOI of the Dataset as assigned by the ANC
- The year the dataset was published or made publicly available
- The version of the dataset in the form of Git tag name or Git commit hash
- The name of the repository in which the dataset is stored and published: Austrian NeuroCloud

For example: Richlan, F., Birklbauer, J., Denissen, M., Pawlik, M., Kronbichler, M., Hutzler, F. (2024). Neuroimaging data from a stop signal task in young amateur soccer players [Data set] (Commit: 70ea5556). Austrian NeuroCloud. https://doi.org/10.31219/osf.io/xdbrv

Additionally, if the Licensor has listed a related scientific publication, the Licensee must also cite that publication in accordance with the provided citation format.

2.7. Any modifications made to the dataset must be thoroughly documented. Documentation should provide sufficient detail to enable reliable reproduction of the modified dataset using standard software tools. The modified dataset must be published under a license that is consistent with the terms of this license, ensuring continuous compliance with its provisions.

## 3. Other rights

3.1. Moral rights, such as the right of integrity, or the right of data protection and/or similar personality rights, shall not be affected by this License. In this sense, the Licensee shall process personal data in accordance with the requirements of the GDPR and any other relevant privacy legislation, as well as comply with any conditions set by the Licensor. However, the Licensor shall waive these rights, including their enforcement, to the limited extent necessary to exercise the transferred rights.

3.2. This License shall not affect patent and trademark rights.

3.3. Provided that there are no legal obligations, the Licensor waives any right to collect royalties for the use of the licensed dataset and metadata, whether directly or through a collecting society under any voluntary or waivable statutory or compulsory licensing scheme.

3.4. The Licensee shall agree not to make any attempts (including for research purposes) to re-identify data or to publish or disseminate re-identified data.

## 4. Exclusion of warranty and liability

4.1. Unless otherwise separately undertaken by the Licensor, to the extent possible, the Licensor offers the licensed dataset for use and editing. No representations or warranties of any kind concerning the licensed dataset are made, whether expressed, implied, statutory, or other, and any warranty, including statutory warranty, shall be excluded. This includes warranties of title, merchantability, fitness for a particular purpose, non-infringement, absence of latent or other defects, accuracy, or the presence or absence of errors, whether known or discoverable.

4.2. To the extent possible, the Licensor shall not be liable for any losses, costs, expenses, or damage arising from this License or from the use of the licensed dataset.

4.3. The exclusion of warranty and limitation of liability provided above shall be interpreted in a manner that, to the greatest extent possible, most closely approximates an absolute disclaimer and waiver of all liability.

## 5. Term and termination

5.1. If you fail to comply with the provisions of this License, the Licensor and the ANC have the right to order the Licensee to stop using the dataset. Use of the dataset must be discontinued immediately upon the first request. The Licensee's access to the dataset will be suspended until the issue has been resolved in consultation with the Licensee and the Licensor. ANC reserves the right to inform the Licensee's employer. These measures are without prejudice to the authority of the Licensor and the ANC to hold the Licensee liable in court in the event of non-compliance or insufficient compliance with this License.

5.2. This section shall in no way affect the Licensor's right to demand compensation for the violation of this License. 

## 6. Final provisions

6.1. This License does not, and shall not reduce, limit, restrict, or impose conditions on any use of the licensed dataset that could lawfully be made without permission under this License and shall not be interpreted to this effect.

6.2. Should any provision of this Agreement be or become legally ineffective or unenforceable, this Agreement as such and the remaining provisions shall remain unaffected and enforceable. The ineffective or unenforceable provision shall be replaced by a provision which comes closest to the economic result of the invalid provision, and which is legally effective and enforceable and reflects as closely as possible the intent of the Parties of the Agreement at the time of concluding this Agreement.

6.3. The terms and conditions of this License shall not be deemed to be a waiver and no failure to comply shall be consented to unless expressly agreed to by the Licensor.

6.4. This License shall in no event be construed as a waiver of any rights regarding the dataset that the Licensor holds on a statutory basis, nor shall it be interpreted to that effect.
