# Metadata Specification

!!! info "History"
   
    :material-tag: **1.0.0** Previous versions can be found in the [Agreement history](./index.md#agreement-history).

Sharing metadata is a critical practice in scientific research and data management. High-quality metadata facilitates the understanding and proper utilization of datasets, enhancing the reproducibility of scientific results and fostering collaboration across disciplines. To uphold these principles, Austrian NeuroCloud adheres to the FAIR (Findable, Accessible, Interoperable, Reusable) data management guidelines. By following the FAIR principles, we ensure that the metadata we share meets the highest standards of data stewardship, promoting greater transparency and efficiency in research.

The metadata specification is based on the Brain Image Data Structure (BIDS), the only data format currently supported in the Austrian NeuroCloud. BIDS is a standardized format that organizes and describes neuroimaging and related data, enabling seamless sharing and analysis. By adopting the BIDS format, we ensure compatibility with a wide range of tools and platforms used in the neuroimaging community, further enhancing the utility and impact of our shared metadata.

We are committed to protecting the privacy of individuals by ensuring that no personal data is ever shared or published. All metadata shared will be anonymized and stripped of any personally identifiable information, in compliance with ethical standards and legal regulations. This commitment safeguards the privacy of research participants while allowing the scientific community to benefit from shared data.

Additionally, we will only share metadata that does not reveal any specific scientific contributions, thereby securing the intellectual property of the scientists.

## Metadata sources

The metadata is categorized into four different levels: dataset, participant, measurement, and event. For each of these levels, specific files in a BIDS-formatted dataset are listed and used as the source of the metadata. We do not publish the files as they are, and in most cases we distribute only a summary of the information they contain. We use POSIX syntax and glob patterns to specify the files and their paths relative to the BIDS dataset root directory `./`.

### Dataset level

The metadata provides a comprehensive description of the dataset as a whole, including its name, authors, identifiers, and related publications.

<div class="overview_table" markdown>

| File pattern | Metadata definition |
| ---- | -------- |
| [`./README.md`](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#readme) | Entire content of the file. |
| [`./CITATION.cff`](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#citationcff) | Entire content of the file. |
| [`./dataset_description.json`](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#dataset_descriptionjson) | Entire content of the file. |

</div>

### Participant level

The metadata provides basic information about the participant's demographics and completed assessment tools.
Participant level metadata is published only in summarized form and never disclosed for individual participants.
Data of individual participants may be stored in a database, but the query results reveal only summarized information about participants.

<div class="overview_table" markdown>

| File pattern | Metadata definition |
| ---- | -------- |
| [`./participants.tsv`](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#participants-file) | Columns and their values representing demographic data of the participants, including but not limited to [age](https://neurobagel.org/dictionaries/#age), [sex](https://neurobagel.org/dictionaries/#sex), and [diagnosis](https://neurobagel.org/dictionaries/#diagnosis). Columns representing the availability and a score of a completed [assessment tool](https://neurobagel.org/dictionaries/#assessment-tool). |
| [`./participants.json`](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#participants-file) | Definitions of the columns representing demographic data and the availability of a completed assessment tool. |

</div>

### Measurement level

The metadata provides information about the data modalities available in a dataset, their specific acquisition parameters, and the number of acquisitions within and between multiple sessions. Acquisition times and dates are not published.

<div class="overview_table" markdown>

| File pattern | Metadata definition |
| ---- | -------- |
| [`./sub-*/*sessions.tsv`](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#sessions-file) | The number of different acquisition sessions. |
| [`./sub-*/ses-*/*scans.tsv`](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#scans-file) | Data modalities within a session. |
| `./sub-*/ses-*/*/*.json` | Acquisition parameters of any modality. |

</div>

### Event level

The metadata lists and describes the events that occurred during the data acquisition, in particular the stimuli to which the subjects were exposed. This metadata is essential for semantic interoperability of the datasets, especially for finding similar experiments. This metadata will be published in a summarized form, and we will not redistribute the entire event logs.

<div class="overview_table" markdown>

| File pattern | Metadata definition |
| ---- | -------- |
| [`./sub-*/ses-*/*/*events.tsv`](https://bids-specification.readthedocs.io/en/stable/modality-specific-files/task-events.html) | Stimuli and subject actions. |
| [`./*events.json`](https://bids-specification.readthedocs.io/en/stable/modality-specific-files/task-events.html) | Descriptions of events and their HED annotations. |

</div>

## Dissemination channels

Metadata dissemination is facilitated through several channels, as outlined in the following table. These channels are designed to ensure broad and effective access to the metadata and the underlying datasets, and to enhance its findability within the scientific community.

<div class="overview_table" markdown>

| Channel | Description | Metadata levels |
| ------- | ----------- | --------------- |
| _DOI registration_ | The Austrian NeuroCloud uses [DataCite](https://datacite.org/) as DOI registration service via the Library of the Paris Lodron University of Salzburg. Certain minimal dataset-level metadata is [required for registering a DOI with DataCite](https://datacite-metadata-schema.readthedocs.io/en/4.5/properties/overview/#mandatory-properties). All applicable dataset-level metadata will be shared with DataCite. | Dataset |
| _DataCite Commons_ | [DataCite Commons](https://support.datacite.org/docs/datacite-commons) exposes the metadata of the DOIs registered with DataCite for querying. The metadata of each DOI registered with the Austrian NeuroCloud will be exposed in DataCite Commons. | Dataset |
| _Dataset website_ | Each dataset has an automatically generated website. This website is associated with the dataset DOI. The metadata is rendered on the website and embedded in its source code, complying with the [FAIR assessment guidelines](https://www.f-uji.net/index.php?action=methods) and ensuring a high fairness score. | Dataset, Participant, Measurement |
| _ANC querying interface (work in progress)_ | To increase findability, the metadata will be stored in a database and exposed in a custom-built ANC querying interface. | All levels |
| _Neurobagel node_ | To enable interoperability with other datasets at a participant level, the metadata will be stored in a database and exposed for querying using [Neurobagel](https://neurobagel.org/). | Dataset, Participant |

</div>
