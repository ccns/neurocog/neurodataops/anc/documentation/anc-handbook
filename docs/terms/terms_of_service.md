# Terms of Service

!!! info "History"
   
    :material-tag: **1.0.0** Previous versions can be found in the [Agreement history](./index.md#agreement-history).

!!! abstract "What are the Terms of Service? "

    The Terms of Service are a contract between the Austrian NeuroCloud (ANC) and any User with an active ANC account.

    <h4>Overview</h4>

    - These terms cover how the ANC services are used.
    - These Terms of Service apply in addition to [GitLab Terms of Use](https://about.gitlab.com/terms/).

    <h4>User Responsibilities</h4>

    - Users must register an account, comply with legal standards, avoid illegal activities, and not misuse the ANC's infrastructure.
    - Users agree to inform the ANC about any publications related to the datasets, helping maintain transparency and support ongoing research.
    - Breaking the rules may lead to termination of the user's account.

    <h4>Non-Warranty and Liability</h4>

    - The ANC does not guarantee that datasets will always be available or preserved. Datasets can be deleted at any time without explanation.
    - The ANC does not own the datasets and is not responsible for their content. Use of the ANC services is at the user's own risk.

    <h4>Modifications</h4>

    - The ANC can change these terms and will inform users about major changes.

## 1. Preamble

1.1. The Austrian NeuroCloud (ANC) is an online research data repository for Cognitive Neuroscience data, which stores datasets according to the FAIR principles. The ANC provides Users with the opportunity to store, share, and use datasets and related services. The ANC fully supports the mission of the European Open Science Cloud (EOSC) and is committed to the European Union's open science policy, legal standards, and best open science practices.

1.2. The Depositor is in possession of the datasets that are intended for archiving and reuse. In the context of this Agreement, the term "datasets" shall refer to immaterial goods that are available in digital format and are, as such, merchantable irrespective of a carrier, particularly through the internet. This particularly applies to any type of data as well as accompanying documents and metadata.

1.3. The subject of this Agreement comprises the services of the ANC.

1.4. Persons who are registered with the ANC and have an active account are hereinafter referred to as "Users".

1.5. Persons who are not Parties of this Agreement are hereinafter referred to as "Third Parties".

1.6. By registering with the ANC you accept these Terms of Service.

## 2. Non-warranty

2.1. ANC shall not give any warranty concerning the availability and scope of the services and functions that are being provided, particularly with regard to the preservation, provision and duration of the availability of the datasets, nor can Users derive any legal right of use.

2.2. ANC shall be entitled to delete datasets that have been stored and made available to Users by Depositors, at any time and without stating reasons for the said deletion.

2.3. ANC shall not be liable for the loss of datasets or parts thereof. Should the services be discontinued for an important, unavoidable reason, ANC shall make endeavors to preserve the datasets to the greatest extent that is possible.

## 3. Use of Services

3.1. To use the ANC, it is necessary to register a User-Account.

3.2. When registering for an account, the User must provide the correct and legal name. The User is responsible for ensuring that the information provided remains accurate and up to date throughout the use of the services. If the legal name changes or any other registration details require modification, the User agrees to promptly update the account information.

3.3. The User shall agree to behave lawfully when using the services and when registering an account and shall agree not to pursue any illegal purposes, not to commit any copyright violations or violations of personal rights. The User shall agree not to make any attempts (including for research purposes) to re-identify data or to publish or disseminate re-identified data. The User shall agree not to make any attempts or take any measures that result in the circumvention of the service restrictions put in place by ANC. This particularly applies to restrictions of access to datasets. The User shall agree to hold harmless and indemnify ANC against any claims asserted by Third Parties due to (negligent or willful) unlawful use.

3.4. The User shall agree not to use the IT infrastructure provided by ANC disproportionately or to an extent that exceeds the level of normal use. ANC shall be entitled to define what extent of use shall be deemed disproportionate. For further information and additional terms of use, please refer to the ANC [Code of Conduct](./code_of_conduct.md). These terms are integral to our overall Terms of Service and must be adhered to by all Users.

## 4. Information obligations concerning publications relating to datasets

ANC shall be informed about any publications that are related to the datasets. This shall ensure the traceability of the use of services and thus their preservation and encourages research with the data. ANC shall be informed of any publications that utilize, reference, or are otherwise connected to datasets that are accessible at the ANC, as soon as such a publication is in print, and, at the latest, two weeks following its publication.

## 5. Liability

Any datasets that have been stored and made available do not originate from ANC. ANC only stores them and makes them available on behalf of the Depositors. ANC does not assume ownership of them and dissociates itself from their contents and specific way of presentation. ANC shall not be responsible for any datasets that have been stored and made available and does not assume any warranty for other contents and activities of Users. ANC does not examine, inspect, and monitor the datasets that have been stored and made available with regard to their lawfulness, especially with regard to the immaterial law situation; this particularly applies to protectability and the holding of rights. The use of the service is solely at the Users risk.

## 6. Modification & Severability of these Terms

ANC reserves the right to unilaterally change this agreement. In the event of substantial changes, ANC will inform the User before the new conditions take effect, so that the User has the opportunity to become aware of the changes. The continued use of the services following the posting of any changes to the agreement constitutes acceptance of those changes. If the User does not want to accept the changes, he is no longer entitled to use the ANC.

## 7. Concluding points

7.1. Failure to comply with these Terms of Service in general may result in an extraordinary termination of contract. This may result in the immediate deletion of the User Account, permanently or temporarily, depending on the severity of the misconduct at the discretion of the ANC staff. However, it is imperative to indicate that data Users are also subject to further applicable law most prominently in the area of data protection law and intellectual property rights, and that certain actions of non-compliance may impose severe legal sanctions.

7.2. Should any provision of these Terms of Service be legally ineffective or unenforceable, these Terms of Service as such and the remaining provisions shall continue to be effective and enforceable. Any ineffective or unenforceable provision shall be replaced by a provision which is as close as possible to the economic result of the legally ineffective or unenforceable provision, and which is legally effective and enforceable and as closely as possible reflects the intent of the Parties to these Terms of Service at the time of entering into the said Terms. The same shall apply in the event of a loophole in these Terms of Service.

7.3. Any legal questions arising from or relating to these Terms of Service, including the question of their effective conclusion and their effects produced in advance or afterwards, shall be governed by Austrian law, excluding its rules of conflict of laws.

7.4. The Parties to these Terms of Service agree that, in the event of any disputes due to or in connection with these Terms of Service, including the question of their effective conclusion and their effects produced in advance or afterwards, the court with competence for Salzburg, Austria, regarding the subject matter shall be the exclusive place of venue. A-5020 Salzburg, Austria, shall be the exclusive venue in accordance with the Brussels I Regulation.
