# Code of Conduct

!!! info "History"
   
    :material-tag: **1.0.0** Previous versions can be found in the [Agreement history](./index.md#agreement-history).

The Austrian NeuroCloud (ANC) repository operates on [GitLab](https://about.gitlab.com/), providing a flexible platform for managing datasets and workflows. To ensure that this flexibility is used responsibly and consistently, we have established this Code of Conduct that outlines expected behaviors and practices for all users.

While certain technical restrictions cannot be enforced directly through the platform, the Code of Conduct ensures that users adhere to agreed-upon guidelines that promote organization, security, and efficient collaboration across the repository.

## User account

- Users must not share accounts with anyone.
- [Two-factor authentication (2FA)](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html) is mandatory and enabled by default.
- Users must not create projects other than BIDS dataset projects. For personal projects use, for example, [gitlab.com](https://gitlab.com).

## Groups structure

- [BIDS Datasets group](https://data.anc.plus.ac.at/bids-datasets/) is the top-level group for BIDS datasets.
- [BIDS Datasets group](https://data.anc.plus.ac.at/bids-datasets/) has research unit subgroups.
- Research unit groups are created by the ANC.
- A research unit group must not have further subgroups.
- A research unit group must be public.
- A research unit group must have [a descriptive name and a concise URL](https://docs.gitlab.com/ee/user/group/index.html#create-a-group).
- A short description of a research unit group, including its website URL, is recommended.

## Dataset projects

- A dataset project must belong to one research unit group in [BIDS Datasets group](https://data.anc.plus.ac.at/bids-datasets/). If there is no suitable group in the ANC, one will be created.
- A dataset project must be [created with ANC project templates](../anc-basics/create_dataset_project.md). The templates ensure the correct data format and files necessary for executing ANC operations.
- Contents of a dataset project must obey [BIDS specification](https://bids-specification.readthedocs.io/en/stable/), including additional format requirements, as specified in [Data Format Requirements](./data_format_requirements.md).
- Necessary updates to the data should be made to follow any changes to BIDS specification or [Data Format Requirements](./data_format_requirements.md), as indicated by ANC Data Stewards.
- Data derivatives should not be stored directly in a BIDS dataset. The ANC is working on a systematic solution to store derived data.
- A dataset project must be private until the verification process for sharing with general public, as defined in [Public Data Sharing Policy](./public_data_sharing_policy.md), is completed.
- A dataset project must have [a descriptive name and a concise slug](../anc-basics/create_dataset_project.md).
- [Short description](../anc-basics/create_dataset_project.md) of a dataset project is recommended.
- Dataset projects are managed using [Git](https://git-scm.com/) to track changes over time. The main version (`main` branch) of the project should always be kept error-free, meaning changes that cause issues, like breaking BIDS validation, must not be included.

## User roles and project membership

- Users are granted access to dataset projects with a specific [role](https://docs.gitlab.com/ee/user/permissions.html#roles), which defines user [permissions](https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions) in that project. When a user is added as a member to a dataset project, the lowest necessary role should be assigned. Developer is the highest recommended role.
- Users can be granted access to an entire research unit group. This grants the user access with the same role to all dataset projects in the group, including all projects created in the future.

## Continuous integration

- ANC uses [GitLab's continuous integration (CI/CD)](https://docs.gitlab.com/ee/ci/) to continuously validate data format of any change made to the dataset. Using this feature is strictly reserved to ANC operations. Users must not use it for personal purposes.
- Do not change the CI/CD configuration file `./.gitlab-ci.yml` yourself, as it may break ANC operations executed on the dataset.
