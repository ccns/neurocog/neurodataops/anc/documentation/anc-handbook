# Transfer and License Agreement

!!! info "History"
   
    :material-tag: **1.0.0** Previous versions can be found in the [Agreement history](./index.md#agreement-history).

!!! abstract "What is the Transfer and License Agreement?"

    This agreement is a contract between two parties:

    - **Depositor**: The person who owns the digital research datasets.
    - **University of Salzburg**: The institution that stores and shares the datasets for the Depositor using the Austrian NeuroCloud (ANC) repository.

    <h4>Purpose of the agreement</h4>

    This agreement sets the rules for storing and sharing research data in the ANC. It ensures that both parties know their rights, responsibilities, and comply with legal and ethical standards.

    <h4>Licensing and access</h4>

    - **Metadata** (information about the datasets): The Depositor agrees to share the metadata publicly under the [CC0 1.0 license](https://creativecommons.org/publicdomain/zero/1.0/legalcode), meaning anyone can use it freely, but it must not include [personal data](./public_data_sharing_policy.md).
    - **Datasets**: The Depositor decides who can access the data - either only specific ANC users or the general public. Datasets are shared under [ANC License](./anc_license.md). Public datasets must not contain [personal data](./public_data_sharing_policy.md).

    <h4>Responsibilities of the Depositor</h4>

    - The Depositor grants the ANC the right to use the dataset for the purposes of the repository.
    - The dataset must meet [ANC Data Format Requirements](./data_format_requirements.md).
    - The Depositor must have the legal rights to share the data, ensuring no one else's rights are violated.
    - Data must comply with data protection laws (European [GDPR](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A02016R0679-20160504), Austrian [DSG](https://www.ris.bka.gv.at/GeltendeFassung.wxe?Abfrage=Bundesnormen&Gesetzesnummer=10001597) and [FOG](https://www.ris.bka.gv.at/GeltendeFassung.wxe?Abfrage=Bundesnormen&Gesetzesnummer=10009514)) and good academic practices, especially regarding personal data.
    - The Depositor agrees to protect the ANC from any legal issues that may arise from using the data, for example, copyright issues.
    - The Depositor agrees to behave lawfully when using the ANC services.

    <h4>Responsibilities of the ANC</h4>

    - The ANC will store the datasets securely and provide access according to the Depositor's wishes.
    - The ANC may modify the datasets for quality control or technical reasons, but will inform the Depositor of any changes.
    - The ANC can remove datasets if there are technical or legal issues.
    - The ANC does not guarantee the availability of the datasets or certain functions.
    - The ANC follows all relevant legal regulations, especially data protection laws, and ensures that personal data is handled correctly.

    <h4>Other Important provisions</h4>

    - The agreement continues indefinitely but can be terminated by the ANC under certain conditions.

## 1. Preamble

1.1. This contract is entered into by and between the organization or person authorized to transfer and deposit the digital dataset(s), hereafter referred to as the "Depositor" and University of Salzburg (Kapitelgasse 4-6, 5020 Salzburg, Austria) Austrian NeuroCloud (anc@plus.ac.at), hereafter referred to as "ANC", hereinafter also referred to collectively as "Parties to the Agreement".

1.2. The Austrian NeuroCloud (ANC) is an online research data repository for Cognitive Neuroscience data, which stores datasets according to the FAIR principles. The ANC provides Users with the opportunity to store, share, and use datasets and related services. The ANC fully supports the mission of the European Open Science Cloud (EOSC) and is committed to the European Union's open science policy, legal standards, and best open science practices.

1.3. The Depositor is in possession of the datasets that are intended for archiving and reuse. In the context of this Agreement, the term "datasets" shall refer to immaterial goods that are available in machine-readable formats and are, as such, merchantable irrespective of a carrier, particularly through the internet. This particularly applies to any type of data as well as accompanying documents and metadata.

1.4. The subject of this Agreement comprises the transfer to ANC as well as the potential use of the datasets.

1.5. Persons who are registered with ANC and have an active account are hereinafter referred to as "Users".

1.6. Persons who are not Parties of this Agreement are hereinafter referred to as "Third Parties".

1.7. ANC organizes datasets in Groups, so that, every dataset belongs to exactly one group.

1.8. In addition to this Agreement the ANC [Terms of Service](./terms_of_service.md) shall apply.

## 2. License

2.1. The Depositor shall license the Metadata, as defined in the [Metadata Specification](./metadata_specification.md) under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/legalcode).

2.2. If the Depositor intends to provide access to the dataset exclusively to specific Users of the ANC, a defined group within the ANC, or the general public, the dataset must be licensed under the ANC License. By agreeing to this provision, the Depositor confirms they have reviewed the terms of the ANC License and agrees to make the dataset available in accordance with those terms.

## 3. Deposition

3.1. The Depositor shall transfer the datasets to ANC in accordance with the following provisions.

3.2. The Depositor declares that the dataset corresponds to the specification agreed upon with ANC [Data Format Requirements](./data_format_requirements.md). The Depositor will supply datasets by means of a method, format and medium deemed acceptable by ANC.

3.3. The Depositor declares that he is entitled to dispose of the rights that have been transferred under this Agreement and that the subject of this Agreement is not encumbered by any rights of Third Parties. At present, no rights of Third Parties that would be contrary to granting the said rights are known to the Depositor.

3.4. The Depositor hereby gives an assurance that all and any data protection regulations and any of the public regulations have been considered when collecting, compiling, and transferring the dataset. Therefore, the Depositor is responsible himself for ensuring that all legal regulations are met when collecting, compiling, and transferring the dataset.

3.5. The Depositor hereby gives an assurance that the datasets have been collected or created in conformity with the principles of good academic practice and in conformity with ethical principles.

3.6. The Depositor shall indemnify ANC against any claims, particularly claims due to copyright violations or violations of personal rights, as well as the right to data protection, that Third Parties might assert against ANC due to the use of the said rights as agreed upon. In this respect, the Depositor shall also bear the cost of any legal procedures that ANC may incur in connection with contesting claims asserted by Third Parties.

3.7. The Depositor shall immediately notify ANC of any actual or impending violation or infringement of the rights granted to ANC under this Agreement.

3.8. The Depositor shall agree to behave lawfully when using the services and when registering an account, and shall in particular agree not to pursue any illegal purposes, not to commit any copyright violations or violations of personal rights, and especially not to disseminate any datasets that:

- are unlawful, threatening, abusive, harassing, defamatory, libellous, deceptive, fraudulent, invasive of another's privacy, tortious, obscene, offensive, or profane;
- that contain unauthorized or unsolicited advertising;
- contain software viruses or any other computer codes, files, or programs that are designed or intended to disrupt, damage, limit or interfere with the proper function of any software, hardware, or telecommunications equipment or to damage or obtain unauthorized access to any system, data or other information of ANC or any Third Party; or
- imposes or may impose an unreasonable or disproportionately large load on ANC’s or its Third Party providers' infrastructure.

3.9. The Depositor shall agree not to use the IT infrastructure provided by ANC disproportionately or to an extent that exceeds the level of normal use. ANC shall be entitled to define what extent of use shall be deemed disproportionate.

3.10. The deposited data use the infrastructure and services of the ANC. Scientific publications that use the ANC to deposit and disseminate data shall provide appropriate attribution to the repository by citing the publication listed on the [ANC website](https://anc.plus.ac.at/publications.html#citing-the-austrian-neurocloud).

3.11. For further information and additional terms, please refer to the ANC [Code of Conduct](./code_of_conduct.md). These terms are integral to this Agreement and must be adhered to by the Depositor.

## 4. Archiving

4.1. ANC will store the dataset and, in accordance with the licenses granted by the Depositor, make them available to the general public or specified Users within the ANC.

4.2. Furthermore, ANC can make datasets (or substantial parts thereof) available to Third Parties if ANC is required to do so by legislation or regulations, a court decision, or by a regulatory or other institution, if this is necessary for the preservation of datasets or (to a similar institution) if ANC ceases to exist and/ or its activities in the field of data-archiving are terminated (see 4.8.).

4.3. ANC will view and examine the dataset with regard to comprehensibility of the documentation as well as conformity with technical requirements as to the formats in which the dataset has been transferred, stored, and made available. Should it become apparent while viewing the dataset that they are not suitable for archiving, ANC shall not be obliged to include the said dataset in its archives and can return or retransmit them to the Depositor within an appropriate period of time.

4.4. The Depositor shall grant ANC the unilaterally irrevocable, non-exclusive right, free of charge and unlimited with regard to time and place, to use the dataset transferred in full or in part at its discretion, and in particular to reproduce, publish, disseminate, send, archive, make available to the general public, edit the said dataset, and in particular combine them with other datasets and Metadata and carry out any modifications that are necessary for assuring the quality of the said dataset and Metadata or for technological reasons or in regard to archiving requirements. ANC shall inform the Depositor of any such changes. ANC shall not guarantee the correctness of the data included in the dataset and will not assume ownership due to such an examination. The Depositor shall also grant ANC those rights of use that will be necessary due to future technological developments or changes in the applicable legislation.

4.5. Should the dataset be modified by the ANC, whether before or after it is made available to the general public or specified Users of the ANC, the provisions of this Agreement shall also apply to all modified versions of the said dataset.

4.6. ANC shall be entitled to delete or restrict or prevent access to datasets and the Metadata on a temporary or permanent basis whenever they are not suitable for archiving, or for being made available. Should the dataset and Metadata be blocked or deleted, fundamental Metadata that indicate the prior existence of the said dataset and Metadata will continue to be visible. The Repository shall inform the Depositor in such cases.

4.7. ANC does not examine, inspect, and monitor the datasets that have been stored and made available with regard to their lawfulness, especially with regard to the immaterial law situation; this particularly applies to protectability and the holding of rights. Should ANC become aware of any unlawful actions or unlawful datasets, or should ANC become aware of any circumstances that clearly indicate unlawful activities or unlawful datasets, ANC shall be entitled to remove the said datasets immediately, to block access to them and inform responsible authorities.

4.8. After the dataset and Metadata have been transferred and stored, the Depositor can no longer demand the deletion of the dataset and Metadata, unless the Depositor can assert a serious reason that cannot be complied with by blocking the said dataset and Metadata. The ANC reserves the right to assess the seriousness of the claimed reason.

4.9. To ensure the long-term storage and availability of the transferred dataset and Metadata, ANC shall be entitled to conclude Agreements and take measures to that effect. The Depositor shall agree that the rights under this Agreement may be transferred for the said purpose at any time. This shall particularly apply if ANC is dissolved or can no longer pursue its original objective. If ANC ceases to exist or terminates its data-archiving activities, ANC shall attempt to transfer the datasets to a similar organization that will continue the Agreement with the Depositor under similar conditions, if possible, inform the Depositor of the cancellation of its activities and provide the Depositor the opportunity to transfer a copy of the dataset. ANC reserves the right to commission Third Parties to make the said dataset and Metadata available. 

4.10. ANC shall ensure, to the best of its ability and resources, that the deposited dataset is stored in a sustainable manner for the duration of this Agreement.

4.11. Archiving will take place without giving any warranty, particularly with regard to the duration of archiving, the availability of the dataset and Metadata and scope of the services and functions that are being provided, nor can Depositors derive any legal right of use. ANC is not liable for any damage or losses resulting from acts or omissions by Users or Third Parties to whom the content has been made available.

4.12. The Depositor will ensure the availability of a stable email address and check it regularly so that permission requests can be processed within a reasonable period of time. ANC will facilitate consultations between the Depositor and the User who wants to use a dataset but cannot be held responsible for the Depositor’s decision whether or not to make the dataset available, nor for any conditions under which this is done.

4.13. The datasets will each be assigned a Digital Object Identifier (DOI). This is a persistent hyperlink which can be used to reference the dataset. The DOI will be reserved if the quality requirements (See 3.2.) are met and will become active after publication of the dataset.

## 5. Availability of the datasets

5.1. ANC shall publish the [Metadata](./metadata_specification.md) provided by the Depositor directly to the general public. The ANC will make the Metadata available under the conditions of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/legalcode) (see 2.1).

5.2. Access to the dataset is either only granted to Users of the ANC ("restricted access") or to the general public ("open access").

5.3. If restricted access is applied, the ANC shall make the dataset available only to Users who have agreed to comply with the [Terms of Service](./terms_of_service.md) and the [ANC License](./anc_license.md) laid down by ANC. Furthermore, if the Depositor chooses, they can grant the User access to an entire Group of datasets. This access will then extend to all currently datasets within the Group, as well as any future datasets added to it.

5.4. By default, datasets are not accessible to the general public. However, Depositors may opt to share the dataset with general public, subject to verification and approval by the ANC.

5.5. If the files in the dataset or the Metadata, or parts of them, contain personal data within the meaning of the GDPR, the files must not be made available directly to the general public.

## 6. Final Provisions

6.1. This Agreement shall enter into force after being accepted by the Depositor and shall be of unlimited term and may be exclusively terminated by ANC. ANC reserves the right to continue to store the datasets after termination of this contract.

6.2. Failure to comply with this Agreement in general may result in an extraordinary termination of contract. This may result in the immediate deletion of the account of the Depositor, permanently or temporarily, depending on the severity of the misconduct at the discretion of the ANC staff. However, it is imperative to indicate that Depositors are also subject to further applicable law most prominently in the area of data protection law and intellectual property rights, and that certain actions of non-compliance may impose severe legal sanctions.

6.3. The Parties to the Agreement shall, in accordance with the applicable provisions of data protection law, agree to a mutual communication of data to the other Party and to the processing of the said data by the Party, both during the term of this Agreement and afterwards, to enable the Parties to the Agreement to examine compliance with the terms of this Agreement.

6.4. This Agreement shall conclusively define the legal relationship between the Parties to the Agreement with regard to the content of this agreement. Upon signing this Agreement, any other Agreements concluded by the Parties of this Agreement, or declarations of intent or confirmations of notice given before or at the time of the conclusion of this Agreement, as well as any other circumstances of legal relevance, shall cease to be effective.

6.5. Any notification stipulated under this Agreement or provided by law shall be deemed delivered on the day on which the said notification has arrived at the respective email address of the other Party to the Agreement.

6.6. Any change of email address or postal address shall expressly be communicated to the other Party to the Agreement in writing and shall become effective one week after arrival at the address of the other Party to the Agreement.

6.7. ANC reserves the right to unilaterally change this Agreement. In the event of substantial changes, ANC will inform the Depositor before the new conditions take effect, so that the Depositor has the opportunity to become aware of the changes. The continued use of the services following the posting of any changes to the Agreement constitutes acceptance of those changes. If the Depositor does not accept the changes, he is no longer entitled to use the ANC.

6.8. Should any provision of this Agreement be legally ineffective or unenforceable, this Agreement as such and the remaining provisions shall continue to be effective and enforceable. Any ineffective or unenforceable provision shall be replaced by a provision which is as close as possible to the economic result of the legally ineffective or unenforceable provision, and which is legally effective and enforceable and as closely as possible reflects the intent of the Parties to the Agreement at the time of concluding this Agreement. The same shall apply in the event of a loophole in this Agreement.

6.9. Any legal questions arising from or relating to this Agreement, including the question of its effective conclusion and its effects produced in advance or afterwards, shall be governed by Austrian law, excluding its rules of conflict of laws.

6.10. The Parties to this Agreement agree that, in the event of any disputes due to or in connection with this Agreement, including the question of its effective conclusion and its effects produced in advance or afterwards, the court with competence for Salzburg, Austria, regarding the subject matter shall be the exclusive place of venue. A-5020 Salzburg, Austria, shall be the exclusive venue in accordance with the Brussels I Regulation.
