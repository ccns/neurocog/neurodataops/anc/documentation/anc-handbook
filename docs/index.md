# Home

Welcome to the [Austrian NeuroCloud](https://anc.plus.ac.at/) (ANC) handbook. The ANC is an online repository for Cognitive Neuroscience data, which stores actively curated datasets adhering to the FAIR principles.

This handbook provides instructions on how to work with the ANC, how to get access, how to add data, and curate and work on your data. Currently, this project and the handbook are still under development. If you have any questions, feel free to reach out at [anc@plus.ac.at](mailto:anc@plus.ac.at)

<div class="grid cards" markdown>

-   :fontawesome-solid-section:{ .lg .middle } __Terms of Use__

    ---

    Before using the ANC, please review and agree to our Terms of Use. We've taken great care to resolve the legal complexities of managing neuroscientific data, ensuring your research is well-protected.

    [:octicons-arrow-right-24: Go to terms](./terms/index.md)

-   :fontawesome-solid-shapes:{ .lg .middle } __ANC Basics__

    ---

    If you are a first time user of the ANC we recommend you go over the ANC basics. Here we provide information on what the ANC is, how to obtain access, and how to create your first project and start working with the ANC.

    [:octicons-arrow-right-24: Go to basics](./anc-basics/index.md)

-   :fontawesome-solid-list-check:{ .lg .middle } __ANC data formatting guides__

    ---

    To upload your data to the ANC it needs to meet the ANC format requirements. Here you find guides that help with the conversion of specific data types.

    [:octicons-arrow-right-24: Go to data requirements](./data-formatting-guides/index.md)

-   :fontawesome-solid-diagram-predecessor:{ .lg .middle } __Data Acquisition Guides__

    ---

    Our standards for deposited data are high. To help researchers create FAIR datasets we provide detailed acquisition guides, which can be followed from the moment a study is planned.

    [:octicons-arrow-right-24: Go to acquisition guides](./data-acquisition-guides/index.md)

-   :fontawesome-solid-circle-question:{ .lg .middle } __Frequently Asked Questions__

    ---

    If you get stuck, please see our FAQ

    [:octicons-arrow-right-24: Go to FAQ](./faq.md)

</div>
