# Data acquisition guides

Here you find our data acquisition guides. These guides provide step-by-step instructions on what to do during data collection to ensure all your data ends up formatted according to the requirements of the ANC, including all necessary metadata.

No matter what datatype you are collecting, or where, good data management starts with defining exactly what data will be collected when. Once you have an overview, select one of our guides and follow along. We currently provide guides for MRI data, task experiment data (also suitable for task MRI) and questionnaire data.

Our guides are tailored towards users at the University of Salzburg, but could be helpful for outside users as well. For example, our [MRI naming conventions](./salzburg/mri-data/part1/step2_sequence_naming.md#naming-patterns-by-datatypes) are there for anyone interested in using our conversion pipeline.

<div class="grid cards" markdown>

-   :fontawesome-solid-brain:{ .lg .middle } __MRI Data__

    ---

    How to acquire MRI data for the ANC

    [:octicons-arrow-right-24: Go to MRI data acquisition guide](./salzburg/mri-data/index.md)

-   :fontawesome-solid-stopwatch:{ .lg .middle } __Task Experiment Data__

    ---

    How to acquire task experiment data for the ANC (with or without neuroimaging data)

    [:octicons-arrow-right-24: Go to task experiment data acquisition guide](./salzburg/task-experiment-data/index.md)

-   :fontawesome-solid-clipboard-list:{ .lg .middle } __Questionnaire Data__

    ---

    How to acquire questionnaire data for the ANC

    [:octicons-arrow-right-24: Go to questionnaire data acquisition guide](./salzburg/questionnaire-data/index.md)

-   :fontawesome-solid-water:{ .lg .middle } __MEG Data__

    ---

    How to acquire MEG data for the ANC

    [:octicons-arrow-right-24: Go to MEG data acquisition guide](./salzburg/meg-data/index.md)

</div>
