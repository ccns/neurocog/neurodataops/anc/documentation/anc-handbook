# Step 4: Complete the dataset

### Add Metadata

There are automatic issues created for your datasets. Make a single merge request to fill in the README.md, CITATION.cff, dataset_description, participants.tsv and participants.json. Find more information on how this files should look like in [General Metadata](../../../data-formatting-guides/general_metadata.md). For more detailed questions, you can also have a look at the [Bids-Specification](https://bids-specification.readthedocs.io/en/stable/). Sometimes it also helps to look at existing datasets.

###  Check BIDS validator output

To review the output of the BIDS validator, click the first circle on the right side of the pipeline overview:

![Merge request pipeline](../mri-data/images/merge_request_validation_marked.png)

- Click *bids-validate* in the appearing box

You will see the output of the BIDS validator. Pay attention to the following:

- Does the validator report any errors?
- Missing event data is expected
- Make note of errors related to inconsistencies or corrupt data


Make sure your dataset passes the BIDS-Validator pipeline. If it doesn't, fix errors. Always keep in mind, that the [ANC-Team](https://anc.plus.ac.at/team.html) is happy to support you!

Once you have done this, and it looks all good, assign the data steward to review so the data can be merged in.