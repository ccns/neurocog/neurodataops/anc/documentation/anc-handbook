# Step 3: MEG Bids Importer

Follow the instructions in the readme for the usage [here](https://gitlab.com/thht_science_software/meg_bids_importer/-/tree/1-make-readme?ref_type=heads).

Make sure, you have set up [Plus-Sync](https://gitlab.com/thht_science_software/plus_sync)

Here's an extended version of the instructions:

1. Initialize
    You can create a new configuration file (`meg_bids_importer.toml`) by issueing: 

    ```bash
    meg_bids_importer init
    ```

    You will be prompted to provide the ANC project slug and the name of the `plus_sync` remote. For example, if your study acronym is `ns_lipreading`, you are part of the Auditory Neuroscience Lab, and you followed the instructions in Step 2, you would enter the following:

    ```
    anc_project_slug = "bids-datasets/auditory/ns_lipreading"
    plus_sync_remote = "sinuhe"
    ```

    After the initializing a new configuration file at `meg_bids_importer.toml` is done, open it to add missing pieces.

2. A GitLab Project Access Token needs to be created and then stored in a separate file, referenced at `anc_token_file` in the `meg_bids_importer.toml`.

    ```
    anc_token_file = ".gitlab_token"
    ```
    Enter the name of the token file `.gitlab_token` to the .toml file and create it. Get the gitlab token. Therefore on your project on the ANC go to >>`settings`>>`access_tokens`.

    :exclamation: :exclamation: Currently, you do not have permission to get this gitlab token at the ANC. This workflow will be updated.
 

3. Add a task 
    ```bash
    ❯  meg_bids_importer task --help
    ```

    ```bash
        Usage: meg_bids_importer task [OPTIONS] COMMAND [ARGS]...    
        Manage Tasks.                                                                           
                                                                                                
        ╭─ Options ─────────────────────────────────────────────────────────────────────────────╮
        │ --help          Show this message and exit.                                           │
        ╰───────────────────────────────────────────────────────────────────────────────────────╯
        ╭─ Commands ────────────────────────────────────────────────────────────────────────────╮
        │ add                         Add a new task to the configuration.                      │
        │ add-callback-event-config   Add a callback event configuration to a task.             │
        │ add-simple-event-config     Add a simple event configuration to a task.               │
        ╰───────────────────────────────────────────────────────────────────────────────────────╯
    ```

    With `task`, new tasks can be added to the configuration file, and the function to create the event files from the data can be added.  

    For the resting state issue:

    ```bash
    ❯  meg_bids_importer task add resting
    ```


    For the other tasks, you need to write a python function with the raw filename as an input, that returns three elements:

    * Events, that can be extracted with the obob_mne function[`read_events_from_analogue`](https://gitlab.com/obob/obob_mne/-/blob/master/obob_mne/events.py?ref_type=heads) (`np.ndarray`)
    * Event Metadata, that will be saved as the `events.tsv` (`pd.DataFrame`)
    * Description of the columns in the event metadata as keys (`dict`).

    In this case, if the task is called vis and the function is saved in utils/bids_importer.py, I can add the task and the function with this command: 

    ```
    ❯  meg_bids_importer task  add-callback-event-config vis utils.bids_importer.extract_vis_events
    ```

    meg_bids_importer.toml this will then look like this:

    ````
    [[tasks]]
    name = "rest"
    regex = "rest|resting|restingstate"

    [[tasks]]
    name = "vis"
    regex = "visual"

    [tasks.events]
    callback_function = "utils.bids_importer.extract_vis_events"

    ````

4. Upload your data!

    First list the subjects in sinuhe:

    ````
    meg_bids_importer list-subjects sinuhe
    ````

    Select a subject and upload it with:

    ````
    meg_bids_importer upload 19891224hbau
    ````


