# Step 1: Prepare data acquisition

The MEG in Salzburg is maintained by the [Auditory Neuroscience Lab]( https://ccns.plus.ac.at/labs/auditory/). There is a matlab toolbox written on top of psychtoolbox by Thomas Hartmann called [o_ptb](https://o-ptb.readthedocs.io/). We recommend you to write your experiment with o_ptb, for an optimal integration with the lab computer.

However, as long as you have meaningful trigger with exact timing you can inject your data into the ANC (for resting state data, this is not required).

* Set meaningful **trigger**. Find the o_ptb documentation [here](https://o-ptb.readthedocs.io/en/latest/tutorial/o_ptb/triggers_sound.html).
* Plan your event files following the [Task experiment data guide](../../../data-formatting-guides/task_experiment_data.md). Make a draft of your **events.tsv** file to make sure that you have thought of everything you need.
* Make a draft of your **events.json** file following the [Task experiment data guide](../../../data-formatting-guides/task_experiment_data.md).
* What kind of [Demographic Data](../../../data-formatting-guides/demographic_data.md) do you want to acquire? Draft the **participants.tsv** and **participants.json**. If this exceeds the basic form in the lab get in contact with Fred and create a form (online or paper pencil) to get all the information you might need for your analysis.

