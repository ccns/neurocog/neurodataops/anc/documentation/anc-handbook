# MEG data

!!! warning Disclaimer

    The preparation of the MEG data is work in progress. In the following you will be provided with a framework that might be changed slightly in the future and is not worked out completely yet. If you have any questions or suggestions please let us know.


To ensure that data is born FAIR, it is essential to prepare the data collection carefully in advance. This helps to obtain clean and structured data. 

This is an outline of the steps involved in preparing your MEG experiment, to make it easily [BIDS](https://bids-specification.readthedocs.io/en/stable/)-valid and integrate it to the ANC. 


![Workflow](images/ANC_meg.png)


- [**Step 1: Prepare acquisition for a MEG dataset**](step1_prep.md) *How to prepare your study, including setting up your events before data acquisitions starts*
- [**Step 2: Data acquisition**](step2_up.md) *Data acquisition, to get everything in place with the BIDS Importer*
- [**Step 3: MEG Bids Importer**](step3_megbids.md) *Some hints for automatic data conversion using the `meg_bids_importer`*
- [**Step 4: Review and complete data injection**](step4_complete.md) *How to handle newly injected data, how to review and complete the data acquisition*
