# Step 2: Data Acquisition

### 1. Naming 
Use the participant identifier as before, using the mothers name and the participants birth date.

Birthdate `YYYYMMDD` + first and third letter of the mothers first name + the first and the third letter of the mothers birthname.

### 2. Save participant info in the OBOB-MRI-Database
Upload demographics to the [OBOB-MRI-Database](https://mridb.ccns.sbg.ac.at/). 

:exclamation: Soon there will also be an automatic upload of the audiogram data to the mridb.

### 3. Save data to Aquisition workstation ("sinuhe")
Fred will do this. Select the study acronym that you will keep consistent from now on. The study acronym should be your initials + underscore + study type. eg. If your name is Nora Sauer and you are doing a lipreading study, the study acronym will be `ns_lipreading`.

`Subject Identifier:` see Naming

`Block:` A block is a run within a session.  Blocks are usually separated by short breaks during which the participant is asked about their well-being.

`Task:` If there are conditions that differ between the blocks (for example one block is auditory only speech and the other visual speech) this can be defined in the task. 

#### File naming requirements

For a resting state block:

```
19891224hbau_resting.fif
```

If there are two different tasks with each 2 blocks:

```
19891224hbau_aud_block01
19891224hbau_vis_block01
19891224hbau_aud_block02
19891224hbau_vis_block02
```

If the task is in all blocks are the same:

```
19891224hbau_block01
19891224hbau_block02
```

### 4. Create your project on the ANC
[Get the ANC account](../../../anc-basics/get_an_account.md) and get access to your working group (If you don't find your research unit group, please ask to create the group for you by sending an email to anc@plus.ac.at including: your user name, the full name of the group, its abbreviated name used in URLs, and a description). Create your project, named by the study acronym.
