# Step 3: Complete the questionnaires

Please repeat the process for all of your questionnaires. We share a complete example of TAS-26 for reference.

## Complete example

<div class="overview_table" markdown>

| Tabular file | JSON file |
|---|---|
| [toronto_alexithymia_scale.tsv](./resources/toronto_alexithymia_scale.tsv) | [toronto_alexithymia_scale.json](./resources/toronto_alexithymia_scale.json) |

</div>

## Add data to your dataset

In order to have the phenotypic data arranged in accordance with the current [BIDS specification](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#phenotypic-and-assessment-data) add the obtained `<measurement_tool>.tsv` and the corresponding `<measurement_tool>.json` files to a `phenotype` directory, which should be placed directly in your dataset directory. Additionally, ensure the `participant_id` is included in the `<measurement_tool>.tsv` file. Any participant for which you have questionnaire data must be listed in the `participants.tsv` file of the dataset.
