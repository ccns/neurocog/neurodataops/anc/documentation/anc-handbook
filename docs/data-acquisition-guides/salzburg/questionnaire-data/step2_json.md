# Step 2: Create a corresponding `.json` file for an assessment tool

The corresponding `<measurement_tool>.json` file is used to provide a detailed description of the content of the `measurement_tool.tsv` file in a human as well as machine actionable way.

## Schema used for `.json` files

Within the `<measurement_tool>.json` there are two parts. There is a description of the *general questionnaire metadata*. The rest of the file describes the individual columns in the `<measurement_tool>.tsv` file, which includes a description of `participant_id` and the *metadata of the questionnaire items*.

### Example of general questionnaire information

To make data fully reusable some high-level metainformation about the questionnaire is required in the annotations.
This is done via the a `MeasurementToolMetadata` entry in the `<measurement_tool>.json` file and contains a narrative description of the questionnaire (such as the questionnaire name and/or common abbreviations) as well as a TermURL (if available). 

!!! note "TermURL: What is it?"

    TermURLs are persistent identifiers for specific terms and a TermURL is part of a standardized vocabulary. Standardization is especially important when, for example, questionnaires with multiple names (e.g., Progressive Matrices Test, APM, Raven's Progressive Matrices) should represent the same entity. This helps to automate data querying for e.g., a specific questionnaire independent of the actual questionnaire name. 

    Example TermURLs are:  

    - `cogatlas:trm_526af65b16c82` for the Stanford-Binet Intelligence Scales from Cognitive atlas  
    - `snomed:273306008` for the Beck Depression Inventory from SNOMED  

#### How to get a TermURL for my questionnaire

Up to now the ANC supports two standard vocabularies for the TermURLs of questionnaires: [SNOMED](https://www.snomed.org/) and the [Cognitive Atlas](https://www.cognitiveatlas.org/).

To check for a TermURL for the questionnaire you are using, search the lists below (if the entries are not shown immediately please click "Expand All") and add the appropriate TermURL to the `TermURL` field in the `MeasurementToolMetadata` entry in your `<measurement_tool>.json` file. If a term exists in both vocabularies, you are free to choose one. If your questionnaire does not appear in either list, the `TermURL` entry can be deleted.

|    Vocabulary    |        Term List                                  |                
| ------------------- | ----------------------------------------------------------------- | 
| SNOMED    | [:fontawesome-regular-circle-down:](./resources/measurementTermsSNOMED.json)|
| Cognitive Atlas |[:fontawesome-regular-circle-down:](./resources/measurementTermsCOGATLAS.json)|

This is what a full `MeasurementToolMetadata` entry looks like:

```json
"MeasurementToolMetadata": {
    "Description": "Beck Depression Inventory",
    "TermURL": "snomed:273306008" 
}
```

### Example of adding metadata of the participant ID in the `<measurement_tool>.json`.

Each `<measurement_tool>.tsv` file has to include a column called `participant_id` to later identify the responses on the subject level.
The annotation of the `participant_id` column is in line with the [Neurobagel Data Model](https://neurobagel.org/dictionaries/#participant-identifier) and looks like the following:

```json
"participant_id": {
    "Description": "A participant ID",
    "Annotations": {
      "IsAbout": {
        "Label": "Subject Unique Identifier",
        "TermURL": "nb:ParticipantID"
      },
      "Identifies": "participant"
    }
}
```

### Example of adding metadata of a column of the `<measurement_tool>.tsv` to the `<measurement_tool>.json` (Toronto Alexithymia Scale - TAS-26)

Assessment tools usually contain multiple items which may vary in their response style.
For the content of the `<measurement_tool>.json` file we adhere to the recommendations of BIDS that can be found [here](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#phenotypic-and-assessment-data) for the questionnaire specific entries and [here](https://bids-specification.readthedocs.io/en/stable/common-principles.html#tabular-files) for the general fields that can be used for `.json` files accompanying `.tsv` files.

In the survey the first item of the TAS-26 is presented to the participant via the survey software as follows:

![LimeSurvey question](images/pheno-limesurvey-question.png)

To also make this information accessible to people who want to reuse your data we add a detailed description using reproschema elements. The following example concerns an item where participants had to answer on a discrete rating scale between 1 and 5. Since this reflects a distinct value response patters, each level possible for the participant to choose has to be described in the `<measurement_tool>.json` file. 
The json file describes the meaning of all possible values in this column.

=== "TAS-26 `.json` file"

    ```json
    "tas[tas01]": {
        "LongName": "Wenn ich weine, weiß ich immer warum",
        "Description": "Description for tas[tas01]",
        "Levels": {
            "1": "trifft gar nicht zu",
            "2": "trifft eher nicht zu",
            "3": "trifft teilweise zu/teilweise nicht zu",
            "4": "trifft eher zu",
            "5": "trifft nicht zu",
            "6": "Keine Antwort"
        }
    }
    ```

=== "TAS-26 `.tsv` file"

    {{ read_csv('./resources/toronto_alexithymia_scale.tsv', sep='\t', engine='python') }}
  
## JSON templates for different question types

To get started you can use the assessment tool template provide [here](./resources/measurement_tool_template.json). It includes a description of the `participant_id` column required by the BIDS-specification and the template for a single choice item with four response options.

Combine the question templates provided below to insert them item for item to the assessment tool template and create a detailed description of your assessment tool:

<div class="overview_table" markdown>

|    Question type    |                             Template                              |                                                                                                           Comment                                                                                                            |
| ------------------- | ----------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Distinct response values   | [:fontawesome-regular-circle-down:](./resources/ordinal.json)     | Question where the response options are distinct values, such as a Likert-Scale |
| Continuous response or free text response  | [:fontawesome-regular-circle-down:](./resources/freeText.json)                     | In such cases only the `Description` and the `Long Name` of the item are required.               |
</div>
  
Feel free to use any other entity that is allowed by BIDS to annotate tabular files (listed [here](https://bids-specification.readthedocs.io/en/stable/common-principles.html#tabular-files)).

If you have any questions about the proper annotation of your questionnaire, do not hesitate to contact your assigned Data Steward for help.
