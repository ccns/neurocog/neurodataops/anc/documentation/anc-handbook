# Questionnaire data

!!! warning Disclaimer

    The preparation of the phenotype data is work in progress. In the following you will be provided with a framework that might be changed slightly in the future and is not worked out completely yet. If you have any questions or suggestions please let us know.

This is an outline of the steps involved in preparing questionnaire data that comes with your neuroimaging data. In the [BIDS specification](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#phenotypic-and-assessment-data) this type of data is referred to as phenotype data.

- [**Step 1: Create a `.tsv` file for an assessment tool**](step1_tsv.md) *Transform your questionnaire data to a BIDS compatible `tsv` file*
- [**Step 2: Create a corresponding `.json` file for an assessment tool**](step2_json.md) *Add metadata to explain the questionnaire and make it machine actionable*
- [**Step 3: Complete the questionnaires**](step3_complete.md) *Complete all questionnaires and add them to your BIDS dataset*
