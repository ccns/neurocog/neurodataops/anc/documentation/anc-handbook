# Step 1: Subject Registration

The subject identifier must follow this pattern consisting only of alphanumeric strings in lower case. When using our automatic conversion pipeline, make sure your project is created before acquiring the first subject and the `<projectid>` matches the slug of the ANC project (See [create dataset project](../../../../anc-basics/create_dataset_project.md)). Remember session is required even when there is only one session.

``<researchgroup>_<project>_<subid>_<session>``

* ``<researchgroupid>``:

    Use the identifier of your research group.

* ``<projectid>``:

    The identifier of your project/study. This *must* match the slug of your dataset project (See [create dataset project](../../../../anc-basics/create_dataset_project.md)).

* ``<subid>``:

    We suggest using a single letter indicating the group the subject belongs to followed by a three digit number: ``\[a-z\]\\d{3}``. Possible letters might be *s* for subject or *c* for control. An example subid could be *s001*.

* ``<session>``:

    The session label is required even for studies with only one session. Note that within a study all sessions with the same label must contain the same scanning sequences.

The subject's name should be entered as the last name in the patient registration panel:

![subject_registration](../images/subject_registration.jpg){:width='50%'}
