# Step 2: Acquire data

Once everything is set up correctly, data can be collected as usual. However, issues can always arise during data acquisition. If data collection is interrupted or specific data can not be collected as expected it is important to deal with this on site.

## Problem Handling

Document any problem that occurs and how it was handled. Make sure to also keep track of any unusual event during data acquisition. This information should go into the `known issues with the data` or `missing data` section of the `README.md` and migth also be relevant for the `sessions.tsv`.

Two typical problems that may come up during data acquisition that should be handled immediately to prevent issues during conversion are:

1. [Interrupted sequence](#interrupted-sequence)
2. [Interrupted session](#interrupted-session)

### Interrupted sequence

There are many reasons why a sequence on the MR might been interrupted and repeated.

Here we need to differ between three different cases:

#### 1. Sequence restarted

The sequence was stopped and restarted, whereby the second run represent the data you want.

Recommended steps:

* Delete the interrupted sequence
* Export only the run of the sequence you want

For later data processing it seems like the interruption never happened. Still, if the sequence was already running for a while and thus the subject might have an additional training effect in any task, this should be documented.

#### 2. Subject position changed

The sequence was stopped, the subject position changed and then the sequence was restarted.

Recommended steps:

* Run the scout and in case the fmap again
* Delete the interrupted sequence
* Export only the run of the sequence you want
* Export the additional fmap

#### 3. Sequence wasn't repeated

The sequence was stopped, but not repeated. The incomplete data of the interrupted sequence should be used.
A fairly rare case.

Recommended steps:

* Export the interrupted sequence

### Interrupted session

Sessions may be interrupted for various reasons and can be resumed either within the same meeting or in a subsequent meeting.

#### 1. Resumption within the same meeting

A typical example for this use case is if the subject needs to go to the toilet in between.

Recommended steps:

* Do **not end the scanning session** while the subject has left
* When the subject is back in position, repeat the scout and if needed mapping sequences
* Continue with the next incomplete sequence
* Export the session as usual: all complete sequences, no interrupted sequences, and fieldmaps (two sets in case they were reacquired)

#### 2. Resumption at a new meeting

The session terminates. The subject leaves and returns for a follow-up meeting at another day.

Recommended steps:

* Export all completed sequences of the first part of the session
* Do not merge the data into your data repository before the session was completed
* When the session continues, start it with the same acquisition name