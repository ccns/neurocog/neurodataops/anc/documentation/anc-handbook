# Step 3: Export data

Do not export the scouts and phoenix reports, or any interrupted sequences, only export the sequences of interest to *PLUS_EXPORT*.

Carefully check your data before exporting. We provide the following [checklist](../resources/checklist_mri_export.pdf) to ensure all data is exported in the correct format, so that it is ready for automatic conversion. Any problems in the filenames will block conversion, so ensure there are no errors before exporting. If you find a mistake before exporting, they can be corrected.

## Solve wrong subject or sequence Name

**Careful:** Do not change the name during data acquisition!

By altering the subject name while a scanning sequence of that subject is running, you delete the directory the scanner is currently writing the data to. Same is true for altering a sequence name while the sequence is still running.
This will cause an error and a potential system failure.
Thus, only change the name after the whole session of that subject has been completed.

### 1. Go to recent data files

Open the folder structure where you can see the last acquired data (as you would for exporting your data) and select the parent folder of your subject or the sequence you want to alter

![select session](../images/select_session.jpg){width=45%}
or
![select sequence](../images/select_sequence.jpg){width=45%}

It might be, that your data folder and/or subfolder is protected and thus not editable. Make sure to close the participant and all images of it loaded somewhere (viewing, 3D, etc.), the protection then get's removed. If not, it can be removed by right-clicking on the folder, but be careful with this.

### 2. Go to correct

At the top level bar there is a *Edit*.
Select that tool bar and click on *Correct*.

![correct-menu-button](../images/edit.jpg){width=40%}

Several warnings about correcting the subject will pop up.

### 3. Changing the name

Now a pop-up window for changing all subject or sequence information appears.
You can here change the name as well other subject related information like the year of birth for example.
Confirm your alterations at the end.

![correct-pane](../images/edit_pane.jpg){width=55%}

or

![Pane-correct-sequences](../images/pane_correct_sequences.jpg){width=55%}
