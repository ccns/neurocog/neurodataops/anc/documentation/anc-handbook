# Part II: Data acquisition

Setting up your sequences correctly is only the first part of getting your data automatically converted the ANC. Sequence names determine on part of your file name. The other has to be provided during data acquisition. Since unexpected things can happen while acquiring subject data, we also explain how to deal with various issues.

- [**Step 1: Subject Registration**](step1_subject_registration.md) *Fill in the correct information when registering your subject to ensure the right file names*
- [**Step 2: Acquire data**](step2_acquire_data.md) *Data acquisition can continue as normal, however, if something unexpected happens, we explain how to deal with this*
- [**Step 3: Export data**](step3_export_data.md) *Ensure you export the right data with the right file names*
