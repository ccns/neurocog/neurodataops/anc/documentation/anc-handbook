# Example MRI sequences Salzburg

The following tables provide some example sequences that have been used in Salzburg.
For more details view the [exported pdf-file](../resources/anc_templates.pdf){:download} (see [here](./step3_document_sequences.md#export-mri-sequences) how to create such a file for your study).

## Scout

| name                    | head coil | voxel size | slices | TR        | TE     | duration |
| :---------------------: | :---:| :--------: | :----: | :-------: | :----: | :-----: |
| scout_ses-X_acq-aal64loc | 64       | 1.6 mm     | 128    | 3.15 ms   | 1.37 ms | 00:14 |
| scout_ses-X_acq-aal20loc | 20       | 1.6 mm     | 128    | 3.15 ms   | 1.37 ms | 00:14 |

!!! Warning "Scout"

    The scout won't be exported with the data.

## Anatomical sequences

| name                        | suffix | voxel size | slices | TR      | TE      | duration |
| :-------------------------- | :----: | :--------: | :----: | :-----: | :-----: | :---: |
| anat-T1w_ses-X_acq-t1w08mm | T1w    | 0.8 mm     | 208    | 2400 ms | 2.24 ms | 06:38 |
| anat-T1w_ses-X_acq-t1w10mm | T1w    | 1.0 mm     | 176    | 2300 ms | 2.94 ms | 05:58 |
| anat-T2w_ses-X_acq-t2w08mm | T2w    | 0.8 mm     | 208    | 3200 ms | 564 ms  | 05:57 |

## Fieldmap sequences

| name                             | suffix | voxel size | slices | TR      | TE1      | TE2     | intended for | duration |
| :------------------------------- | :-: | :--------: | :----: | :-------: | :----:   | :-----: | :-------: | :---: |
| fmap_ses-X_acq-gefmsl56          | n/a | 2.4 mm     | 56     | 623.0 ms  | 4.92 ms  | 7.38 ms | func-bold_ses-X_task-X_acq-stdmb456sl  | 01:52 |
| fmap_ses-X_acq-gefm30mm48sl     | n/a | 3.0 mm     | 56     | 532.0 ms  | 5.17 ms  | 7.63 ms | func-bold_ses-X_task-X_acq-nomb10mm36sl | 01:17 |

## Functional (BOLD) sequences

| name                                | suffix | voxel size | slices | echos | TR | TE1 | TE2 | TE3 | TE4 |
| :-----------------------------------| :----: | :--------: | :----: | :----:| :-:| :-: | :-: | :-: | :-: |
| func-bold_ses-X_task-X_acq-stdmb456sl | bold | 2.4 mm | 56 | 1 | 1050 ms | 32.0 ms | n/a | n/a | n/a |
| func-bold_ses-X_task-X_acq-mb4mesl56 | bold | 2.5 mm | 56 | 4 | 1299 ms | 12.40 ms | 30.13 ms | 47.86 ms | 65.59 ms |
| func-bold_ses-X_task-X_acq-nomb10mm36sl | bold | 3.0 mm | 36 | 1 |2250 ms | 30.0 ms | n/a | n/a | n/a |

!!! Note "Duration"

     The duration must be adjusted to your task.

## Diffusion weighted image sequences

| name                            | suffix | voxel size | slices | TR      | TE       | duration |
| :------------------------------ | :----: | :--------: | :----: | :-----: | :------: | :---: |
| dwi-dwi_ses-X_acq-256dir_dir-AP | dwi    | 2.0 mm     | 72     | 2490 ms | 99.20 ms | 11:01 |
| dwi-dwi_ses-X_acq-sixdir_dir-PA | dwi | 2.0 mm     | 72     | 2490.0 ms | 99.20 ms | 00:19 |
| dwi-dwi_ses-X_acq-sixdir_dir-AP | dwi | 2.0 mm     | 72     | 2490.0 ms | 99.20 ms | 00:19 |
