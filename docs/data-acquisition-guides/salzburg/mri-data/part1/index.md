# Part I: Prepare acquisition

The names of the data files you export from the MRI are dependent on your sequence names. To ensure your data can be automatically converted to BIDS it is important to properly prepare your sequence names at the scanner.

This guide consist of the following steps:

- [**Step 1: Plan acquisition**](step1_plan_acquisition.md) *Understand what data you plan to acquire and what this means in BIDS terminology*
- [**Step 2: Sequence naming**](step2_sequence_naming.md) *Correctly name all planned sequences*
- [**Step 3: Document sequences**](step3_document_sequences.md) *Save a pdf documenting all sequence parameters*
