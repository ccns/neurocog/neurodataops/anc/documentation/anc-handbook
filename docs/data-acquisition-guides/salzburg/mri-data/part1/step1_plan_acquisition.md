# Step 1: Plan acquisition

Clearly define what data will be acquired. Specify every type of sequence you will obtain. This includes functional, any type of anatomical or diffusion sequences.

If data will be acquired across multiple sessions define which sequences will take place during which session.

*Note*: Although a session is often understood as one visit to the scanner, with the first session representing the first time a participant came to the scanner, in BIDS a session is defined by which data is acquired (see [definition 5 by BIDS](https://bids-specification.readthedocs.io/en/stable/common-principles.html#definitions)). Thus, a session with an identical label must always contain the same data across participants, even when acquired in a different order between participants. If not all data is collected for all participants this is permitted. Dates and times at which different sessions were recorded for each participant will be added to the BIDS dataset at a later step to allow for longitudinal analysis.

If multiple instances of the same sequence will take place, for example multiple functional runs with the same parameters and same duration, define how many and how they will be collected across sessions.

If you are acquiring task based data, BIDS event files are required. See our guide on [task experiment data](../../task-experiment-data/index.md), where we explain how to set up your experiment to output BIDS event files.

When you know what MRI data will be acquired when, for all participants, you can prepare the sequences.