# Step 3: Document sequences

Make sure to document your planned sequences with their parameters properly. This can be done by exporting a file that contains all your sequences with all their settings.

## Export MRI sequences

### 1. Open the Dot Cockpit

On the very right side of the screen in the lower segment there is a register button with a small white arrow to the side.
Click the arrow and another menu point called *Dot Cockpit* appears.
Click to open the Dot Cockpit.

![side-pane](../images/side_pane.png)

### 2. Find your project
  
Find your project in the menu on the left and select the directory.
Make sure to export all sessions of your project into one pdf file by selecting all relevant directories.

![dot-cockpit](../images/export-1.png){width=40%}
![export-2](../images/export-2.png){width=40%}

### 3. Print the file

Right click on the project folder and select *Print...*

Choose the desired format and storage directory (probably your flash drive) and select *print* again.

![export-3](../images/export-3.png){width=40%}
