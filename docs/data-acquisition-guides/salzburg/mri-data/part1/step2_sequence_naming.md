# Step 2: Sequence naming

The sequences must follow the [ReproIn Naming Convention](https://dbic-handbook.readthedocs.io/en/latest/mri/reproin.html) and the [BIDS requirements](https://bids-specification.readthedocs.io/en/stable/appendices/entity-table.html). BIDS filenames are based on BIDS entities (keys), which are paired with a label or index (value). An entity-label pair is connected by a hyphen `-`, e.g. `task-rest`. The entity pairs are separated by underscores `_`, e.g. `func-bold_task-rest`. Correctly naming your sequences depends on knowing what entities to add to your file name and how to label them, which we explain here.

We first discuss some general guidelines about choosing appropriate labels. Next, to help you correctly name the sequences we provide a selection of naming patterns. Mind that not all possible entities are listed per sequence. To view all, have a look at the [BIDS entity table](https://bids-specification.readthedocs.io/en/stable/appendices/entity-table.html). All sequences and entities listed below are supported by our automated data acquisition pipeline.

## General requirements for labels

Every label must be a alphanumeric string in lower case. The only exception is the phase encoding direction where upper case abbreviations are used.

Every index must be numeric.

We recommend the use of the `acq` label to distinguish or describe the sequence further. This label can be used to highlight important parameters (e.g. voxel size) or to maintain inherited sequence tags (e.g. 'hcpli' for the T1w).

If a sequence with the same name is used multiple times within a session, a `run` index based on the chronological order is automatically added by the acquisition pipeline. You can add a `run` index for yourself, but it will be ignored during conversion.

The `task` label has implications for how BIDS apps and curation tools can further process your data. Therefore, pay attention that:

* For resting state fmri sequences `task-rest` must be used.
* Any task data that is not labeled `task-rest` requires valid BIDS events files. See our [guide](../../task-experiment-data/index.md) on how to create these.
* The task label for other tasks should be a short and descriptive name, for example its common name, like *task-stroop* or *task-nback*.

!!! Warning

    Ensure that task data you want to analyze together has the same task label. This makes further processing easier. For example, if you perform different versions of an n-back task but you want to model the neural responses for all these versions together, label the task `nback`. If you are still not certain which task label to use, also consider the event files. If you follow our [guide](../../task-experiment-data/index.md) on structuring event files, tasks with the same label should always have the same columns.

## Naming patterns

The following tables provide the naming patterns. Also see [here](./example_sequences_salzburg.md) for examples.

The requirement level column provides information if an entity needs to be added to the sequence name in any case (required), we recommend to add it (recommended) or if it is optional. Note that optional means we recommended it if applicable. In general, an entity is required when it is necessary to distinguish otherwise identical sequences.

=== "[T1w](https://bids-specification.readthedocs.io/en/stable/glossary.html#t1w-suffixes)"

    `<datatype>-<suffix>_task-<label>_acq-<label>`

    <div class="overview_table" markdown>

    |   Component    |                                                          Input                                                          | Requirement level |
    | -------------- | ----------------------------------------------------------------------------------------------------------------------- | ----------------- |
    | `<datatype>`   | [anat](https://bids-specification.readthedocs.io/en/stable/glossary.html#anat-datatypes)                                | required          |
    | `<suffix>`     | [T1w](https://bids-specification.readthedocs.io/en/stable/glossary.html#t1w-suffixes)                                   | required          |
    | `task-<label>` | [label of experiment task](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#task)           | optional          |
    | `acq-<label>`  | [acquisition, additional information](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#acq) | recommended       |

    </div>

    *e.g.*: `anat-T1w_task-rest_acq-hcpli`

=== "[T2w](https://bids-specification.readthedocs.io/en/stable/glossary.html#objects.suffixes.T2w)"

    `<datatype>-<suffix>_task-<label>_acq-<label>`

    <div class="overview_table" markdown>

    |   Component    |                                                          Input                                                          | Requirement level |
    | -------------- | ----------------------------------------------------------------------------------------------------------------------- | ----------------- |
    | `<datatype>`   | [anat](https://bids-specification.readthedocs.io/en/stable/glossary.html#anat-datatypes)                                | required          |
    | `<suffix>`     | [T2w](https://bids-specification.readthedocs.io/en/stable/glossary.html#objects.suffixes.T2w)                           | required          |
    | `task-<label>` | [label of experiment task](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#task)           | optional          |
    | `acq-<label>`  | [acquisition, additional information](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#acq) | recommended       |

    </div>

    *e.g.*: `anat-T2w_task-rest_acq-hcpli`

=== "[MP2RAGE](https://bids-specification.readthedocs.io/en/stable/glossary.html#mp2rage-suffixes)"

    `<datatype>-<suffix>_task-<label>_acq-<label>_flip-<index>_inv-<index>`

    <div class="overview_table" markdown>

    |   Component    |                                                          Input                                                          | Requirement level |
    | -------------- | ----------------------------------------------------------------------------------------------------------------------- | ----------------- |
    | `<datatype>`   | [anat](https://bids-specification.readthedocs.io/en/stable/glossary.html#anat-datatypes)                                | required          |
    | `<suffix>`     | [MP2RAGE](https://bids-specification.readthedocs.io/en/stable/glossary.html#mp2rage-suffixes)                           | required          |
    | `task-<label>` | [label of experiment task](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#task)           | optional          |
    | `acq-<label>`  | [acquisition, additional information](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#acq) | recommended       |
    | `flip-<index>` | [flip angle: integer](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#flip)                | optional          |
    | `inv-<index>`  | [inversion time: nonnegative integer](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#inv) | required          |

    </div>

    *e.g.*: `anat-MP2RAGE_acq-ssri_flip-10_inv-1`

=== "[fmap: Phase-difference](https://bids-specification.readthedocs.io/en/stable/modality-specific-files/magnetic-resonance-imaging-data.html#case-1-phase-difference-map-and-at-least-one-magnitude-image)" 

    The sequence acquires one phasediff and at least one magnetude image. The corresponding suffixes (phasediff, magnetude1 & magnitude2) will be added automatically during dicom conversion.

    `<datatype>_acq-<label>`

    <div class="overview_table" markdown>

    |   Component   |                                                          Input                                                          | Requirement level |
    | ------------- | ----------------------------------------------------------------------------------------------------------------------- | ----------------- |
    | `<datatype>`  | [fmap](https://bids-specification.readthedocs.io/en/stable/glossary.html#fmap-datatypes)                                | required          |
    | `acq-<label>` | [acquisition, additional information](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#acq) | recommended       |

    </div>

    *e.g.*: `fmap_acq-gefmsl56`

=== "[Functional](https://bids-specification.readthedocs.io/en/stable/modality-specific-files/magnetic-resonance-imaging-data.html#task-including-resting-state-imaging-data)"

    `<datatype>-<suffix>_task-<label>_acq-<label>`

    <div class="overview_table" markdown>

    |   Component    |                                                          Input                                                          | Requirement level |
    | -------------- | ----------------------------------------------------------------------------------------------------------------------- | ----------------- |
    | `<datatype>`   | [func](https://bids-specification.readthedocs.io/en/stable/glossary.html#func-datatypes)                                | required          |
    | `<suffix>`     | [bold](https://bids-specification.readthedocs.io/en/stable/glossary.html#objects.suffixes.bold)                         | required          |
    | `task-<label>` | [label of experiment task](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#task)           | optional          |
    | `acq-<label>`  | [acquisition, additional information](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#acq) | recommended       |

    </div>

    *e.g.*: `func-bold_task-rest_acq-mb4mesl56`

=== "[fmap: 'pepolar'](https://bids-specification.readthedocs.io/en/stable/modality-specific-files/magnetic-resonance-imaging-data.html#case-4-multiple-phase-encoded-directions-pepolar)"

    `<datatype>_acq-<label_dir-<label>`

    <div class="overview_table" markdown>

    |   Component   |                                                          Input                                                          | Requirement level |
    | ------------- | ----------------------------------------------------------------------------------------------------------------------- | ----------------- |
    | `<datatype>`  | [fmap](https://bids-specification.readthedocs.io/en/stable/glossary.html#fmap-datatypes)                                | required         |
     | `<suffix>`     | [epi](https://bids-specification.readthedocs.io/en/stable/glossary.html#dwi-suffixes)                                 | required          |
    | `acq-<label>` | [acquisition, additional information](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#acq) | recommended       |
    | `dir-<label>`  | [phase-encoding direction: e.g. AP, PA, LR...](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#dir) | required         |

    </div>

    *e.g.*: `fmap-epi_acq-6_dir-AP`

=== "[Diffusion Weighted Imaging](https://bids-specification.readthedocs.io/en/stable/modality-specific-files/magnetic-resonance-imaging-data.html#diffusion-imaging-data)"

    `<datatype>-<suffix>_acq-<label>_dir-<label>`

    <div class="overview_table" markdown>

    |   Component    |                                                              Input                                                               | Requirement level |
    | -------------- | -------------------------------------------------------------------------------------------------------------------------------- | ----------------- |
    | `<datatype>`   | [dwi](https://bids-specification.readthedocs.io/en/stable/glossary.html#dwi-datatypes)                                           | required          |
    | `<suffix>`     | [dwi](https://bids-specification.readthedocs.io/en/stable/glossary.html#dwi-suffixes)                                            | required          |
    | `acq-<label>`  | [acquisition, additional information](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#acq)          | recommended       |
    | `dir-<label>`  | [phase-encoding direction: e.g. AP, PA, LR...](https://bids-specification.readthedocs.io/en/stable/appendices/entities.html#dir) | recommended        |

    </div>

    *Note*: Within our conversion pipeline, the Phase Encoding Direction gets retrieved from the DICOM header.

    *e.g.*: `dwi-dwi_acq-256_dir-AP`

## Save sequences on site

Once all sequences are planned they can be prepared on-site. Store project sequences in a designated project directory labeled with name of your data repository.If you have sessions that differ in their sequences, you can have multiple directories, one for each session.
