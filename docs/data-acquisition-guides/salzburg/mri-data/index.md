# MRI data

MRI data is large and complex data. Preparing data acquisition is essential to retrieving clean and structured data. Data collected at the University of Salzburg is automatically converted to BIDS and added to the ANC using our conversion pipeline. This requires that the names of your data files adhere to strict patterns. In the following guide we describe how to set up your data acquisition to ensure your files are names correctly and all necessary metadata is captures along the way. We also explain how to check incoming data for issues and how to correct these when possible.

The MRI acquisition guide consists of three parts:

- [**Part I: Prepare acquisition**](./part1/index.md) *How to prepare your study, including setting up your sequences before data acquisitions starts*
- [**Part II: Data acquisition**](./part2/index.md) *How to ensure that automatic data conversion is possible during data acquisition*
- [**Part III: Review and complete data injection**](./part3/index.md) *How to handle newly injected data, how to review and complete the data acquisition*
