# Checklist data export

[ ] The scanning session was documented  
[ ] All scanning sequences planned for this session have been executed  
[ ] Interrupted sequences were reacquired and the interrupted data has not been selected for export  
[ ] All sequences selected for export follow the correct naming conventions  
[ ] All data of interest has been selected for export  
[ ] The subject identifier follows the correct naming convention ``<researchgroup>_<project>_<subid>_<session>``  
