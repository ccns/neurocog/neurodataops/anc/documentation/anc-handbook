# Part III: Review and complete data injection

Before your data is acquisition is completed it is important to review whether all your data collected as intended, converted correctly, and all associated data and metadata is stored along with it. We recommend this is done as shortly after data collection as possible, as more time in between makes it harder to correct issues and easier for data to get lost.

After you have exported your data a merge request will be generated automatically, containing the obtained data which has been converted to BIDS.

- [**Step 1: Review the incoming data**](step1_review.md) *Review whether the incoming data meets your expectations*
- [**Step 2: Add additional data belonging to this session**](step2_add_data.md) *In most cases you will have additional participant demographic, task, or questionnaire data collected along with your MRI session. Complete your data entry and finalize the data injection*
