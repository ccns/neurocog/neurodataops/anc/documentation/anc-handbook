# Step 2: Add additional data belonging to this session

## 1. Add participant demographic information

In case this is a new participant, add demographic data for this subject.

If you have collected additional participant demographic data besides sex and age we assume you have resolved the relevant template issue and prepared the additional column and its annotation.

Add data and ensure the following:

- There are tabs, not spaces between columns
- There is an empty line at the end of the file
- There are no additional spaces and wrong characters in your file
- For missing data, fill in `n/a`

If you add data and get a merge conflict on your participant file, contact your data steward.

## 2. Add event data

If you collected task MRI data you will have to add your event files in the merge request before it can be merged in. We assume you have generated event files in accordance with our [task experiment data guide](../../task-experiment-data/index.md).

Event data can be further processed later (calculate correct responses, reaction times, etc.). Ensure all necessary information for postprocessing is provided by describing the `events.tsv` file in the `events.json` file.

Add your event files and ensure the following:

- Correctly name the file to match the name of the imaging file, replacing `_bold.nii[.gz]` with `_events.tsv`
- Check bids validation to ensure all events are described in an `_events.json` file
- If applicable, add the original log file of your experiment in the `sourcedata` directory

Add log files following the same directory structure and file name conventions as the converted data:

```txt
.
└── dataset/
    ├── sourcedata/
    │   └── sub-01/
    │       └── ses-01/
    │           └── func/
    │               └── sub-01_ses-01-acq-sqedaddel_events.log
    └── sub-01/
        └── ses-01/
            └── func/
                ├── sub-01_ses-01-acq-sqedaddel_bold.nii
                └── sub-01_ses-01-acq-sqedaddel_events.tsv

```

If you do not have an `events.json` file yet, follow these steps:

- Generate the file [here](https://hedtools.org/hed/events). (Under action, select *Generate sidecar template*. Upload your `events.tsv` file and click *Process*)
- Place this file at the top level of your dataset, naming it task-<yourtaskname>_events.json
- Fill in all *Descriptions*

If a column value is not described in your `events.json`, ensure you add information following appropriate [json syntax](https://www.w3schools.com/js/js_json_syntax.asp). For example:

```json
    "stimulus": {
       "Description": "Description for stimulus",
       "HED": {
          "face": "(Face, Human-agent)",
          "house": "(Building)"
       },
       "Levels": {
          "face": "Centrally presented image of human face",
          "house": "Centrally presented image of a house"
       }
    }
```

If for some subject you presented a third stimulus type *word*, simply extend as such:

```json
    "stimulus": {
       "Description": "Description for stimulus",
       "HED": {
          "face": "(Face, Human-agent)",
          "house": "(Building)",
          "word": "(Word)"
       },
       "Levels": {
          "face": "Centrally presented image of human face",
          "house": "Centrally presented image of a house",
          "word": "Centrally presented German word"
       }
    }
```

!!! Warning

    All event files of the same task should have the same columns. Inconsistencies in this between subjects point to an issue in your task labels. Contact a data steward.

### Add sessions data

In case of multiple sessions, `session.tsv` files ([see BIDS documentation](https://bids-specification.readthedocs.io/en/stable/modality-agnostic-files.html#sessions-file)) acompanied by a `sessions.json` file can be added. This file must contain a `session_id` column and describes changing variables between sessions, for example repeated measures taken such as blood pressure. The [handbook](../../../../data-formatting-guides/sessions_data.md) details how to organize those files.

### Add questionnaire data

If you are acquiring questionnaire data using paper and pencil we recommend you convert it directly and add it to the participant merge request. The [handbook](../../../../data-formatting-guides/questionnaire_data.md) details how your data should be organized.

In general questionnaire data can be added for all participants at once, once data collection is complete. For a step by step guide on how to organize and add your questionnaire data, see the [relevant acquisition guide](../../questionnaire-data/index.md) to correctly organize your phenotypic data.
