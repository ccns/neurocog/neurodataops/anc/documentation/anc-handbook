# Delete Invalid Files

If files that shouldn't be in the repository have been exported, they need to be removed along with their metadata. This requires careful data handling.

!!! Note "Steps to delete invalide files properly:"

    1. Clone Repostory
    2. Delete Invalid Files
    3. Rename Files
    4. Adjust scans.tsv
    5. In case of func data: Correct Metadata of Fieldmaps
    6. Push Changes

### 1. Clone Repository

To operate on LFS files, clone the repository. However, cloning the LFS files themselves isn't necessary; the representative pointer files are sufficient. See the guide on [Cloning a Repository without LFS Files](../../../../anc-basics/working-with-data/download_data.md#cloning-a-repository-without-lfs-files).

!!! warning "Get familiar with GitLab workflow"

    Before you continue, get familiar with our guide on how to [work on data locally](../../../../anc-basics/working-with-data/working_locally.md). 

Once the repository is cloned, navigate to the branch where the changes will be made.

### 2. Delete Invalid Files

Carefully identify which files need to be deleted. For additional validation, use the date- and timestamps in the corresponding `scans.tsv` or the timestamps in the corresponding `.json` files.

Delete the files and commit the changes.

### 3. Rename Files

If the invalid files were an additional run to another sequence, the run labels of the remaining files most properly also need adjustment.

Rename the files and commit the changes.

### 4. Adjust scans.tsv

Apply the changes also to the `scans.tsv`, ensuring the correct date- and timestamps are maintained.

Commit the changes.

### 5. Correct Metadata of Fieldmaps

If the invalid files were functional data, correct the metadata of the corresponding fieldmaps. This involves adjusting the list of functional files under the`IntendedFor` key in the `.json` files.

Remove all files that were deleted and adjust all run labels.

Commit the changes.

### 6. Push Changes

Carefully review all changes made. Ensure all files were correctly renamed and all metadata was adjusted to the new filenames.

Push the commits.
