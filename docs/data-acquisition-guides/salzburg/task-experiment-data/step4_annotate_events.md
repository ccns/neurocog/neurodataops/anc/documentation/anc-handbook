# Step 4: Add events annotations

The ANC requires all columns and values of tabular files to at least have a free text description. Further we recommend adding [Hierarchical Event Descriptor (HED)](https://www.hedtags.org/) annotations. Here we will guide you through the process of adding this information. You can do this step as soon as you have the first `events.tsv` file, whether it is based on the first participant or pilot data.

Use the [HED tools](https://hedtools.org/hed/events) to automatically generate a `.json` file for your `events.tsv` file. 

* Under *Action* select *Generate JSON sidecar template* from the drop down menu
* Under *Input* click *Browse* to upload your `events.tsv` file
* A table will appear with all columns found in your event file. Add all column except the onset column. Mark which columns contain categorical values.
* Click *Process*

After several seconds you will get a json file template. The template will contain a key for each of the columns in your events.tsv file. Depending on whether the column was a value or category column different information for each column is required. 

A category column has the fields `Description`, `Levels`, and `HED`. The `Description` should be a description the content of the column. `Levels` contains multiple fields, each corresponding to a value appearing in a column. The `HED` key also contains multiple fields for each appearing value. Check out the [HED documentation](https://www.hed-resources.org/en/latest/HedAnnotationQuickstart.html) to learn more about adding HED tags. The ANC offers active support for HED annotation so feel free to reach out to your data steward.

A value column has the fields `Description`, `Units`, and `HED`. The `Description` should be a description the content of the column. The `Units` should describe the unit of a values represented as a column. Note that value columns can also be used for individual words, or image files that were presented in an experiment. In this case you can remove the `Units` key. The `HED` field contains a single string in the case of a value column. This should contain at least a HED tag with a `#` to replace with the column value. For more information, see the [HED documentation](https://www.hed-resources.org/en/latest/HedAnnotationQuickstart.html).

```json
{
    "duration": {
        "Description": "Description for duration",
        "HED": "(Label/duration, ID/#)"
    },
    "column_nameA": {
        "Description": "Description for column_nameA",
        "HED": {
            "categoryA": "(Label/column_nameA, ID/categoryA)",
            "categoryB": "(Label/column_nameA, ID/categoryB)",
            "categoryC": "(Label/column_nameA, ID/categoryC)"
        },
        "Levels": {
            "categoryA": "Here describe column value categoryA of column column_nameA",
            "categoryB": "Here describe column value categoryB of column column_nameA",
            "categoryC": "Here describe column value categoryC of column column_nameA"
        }
    },
    "column_nameB": {
        "Description": "Description for column_nameB",
        "HED": "(Label/column_nameB, ID/#)"
    }
}
```

Once you have filled in your descriptions you can add the file to your dataset. You should rename the file to `task-<taskName>_events.json` and place it on the root level of your datasets. BIDS will automatically apply descriptions to all matching `events.tsv` files in your dataset.
