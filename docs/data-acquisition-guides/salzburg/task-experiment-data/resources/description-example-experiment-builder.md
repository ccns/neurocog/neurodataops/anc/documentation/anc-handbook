# Component and routine description of template experiment

Here we provide a detailed description of every component and routine in the [template experiment](https://gitlab.com/ccns/neurocog/neurodataops/anc/templates/psychopy-template).  
The template experiment covers the most basic components in psychopy. If you want to use a component in your own experiment you can look at the description of this component to see how it is created and how to correctly obtain a bids event for the component. We recommend opening the template experiment in psychopy builder and reading the descriptions as you navigate through the bids event.

## The loop

The components are used across several routines, some of which are embedded in a loop.

![View of the Loop properties in the builder.](../images/builder_loop_properties.png)

The loop enables presenting multiple trials and blocks with varying conditions. It can use a tabular input file which specifies variables that should change throughout the experiment. The header of each column is used by Psychopy as the variable name, the value in the respective column is used as the parameter of that variable for a specific iteration of the loop.

## The Routines

|Routine| Components| Description
|---|---|---
|[Welcome Screen](#welcome-screen)|Text, KeyResponse, BIDS task event| Present welcome text until a key is pressed.
|[VisualStimuliText](#visualstimulitext) |Text, BIDS task event| Present text stimulus that varies on every loop.
|[MouseResponse](#mouseresponse) |Text, MouseResponse, BIDS task event| Present text until there is a mouse response
|[VisualStimuliImage](#visualstimuliimage) |Image, BIDS task event| Present an image that varies on every loop
|[SoundStimuli](#soundstimuli)|Sound, BIDS task event|Present a sound that varies on every loop
|[MovieStimuli](#moviestimuli)|Movie, BIDS task event|Present a video that varies on every loop
|[SliderResponse](#sliderresponse)|Text, Slider, BIDS task event|Present a text and a slider and wait for a response via mouse click
|[GoodbyeScreen](#goodbyescreen)|Text, BIDS task event|Present goodbye text
|[Bids Export](#bids-export)|No components|Ensure all BIDS data is saved

### Welcome Screen

![View of the WelcomeScreen routine in the builder.](../images/builder_welcomescreen_routine.png)

This routine consists of two psychopy components, a text component and a keyboard component, and two bids event component to log the events associated with each component:

|Component | Component type| Description | BIDS event
|---|---|---|---
|*text_welcome*|[Text component](https://www.psychopy.org/builder/components/text.html)|Presents a fixed text message.|For the text component we log the onset, duration and presented text. *Onset* is stored by psychopy in the variable  `text_welcome.tStartRefresh`. The *duration* of the text component is equal to the response time on the key press, since the text component does not end until a key press. This is stored in psychopy as `key_resp_welcome.rt` Since the presented text is long, and does not fit into the tabular file we only add an identifier for the full text. The full text should later be added to the `_events.json` file. The identifier is added as a *custom column* under the "More" tab,  in the form of a python dictionary: `{"presented_text":"welcome_txt"}`.
|*key_resp_welcome*|[Keyboard component](https://www.psychopy.org/builder/components/keyboard.html)|Records a keyboard response.|For the keyboard component we log the onset, and the pressed key. The time stamp of the *onset* of the response has to be calculated and is equal to the time at which the stimulus is presented `text_welcome.tStartRefresh` plus time until the key press `key_resp_welcome.rt`. The pressed key is stored in `key_resp_welcome.keys`. We add it under *Custom columns* in the "More" tab as a python dictionary: `{"key_responded":key_resp_welcome.keys}`.

### VisualStimuliText

![View of the VisualStimuliText routine in the builder](../images/builder_visualstimulustext_routine.png)

This routine consists of one psychopy component, a text component, and one bids task event to log the event created by the text component.

|Component|Component type|Description |BIDS event
|---|---|---|---
|*text_word*|[Text component](https://www.psychopy.org/builder/components/text.html)|Presents text read from the stimuli file|For the text component we log the onset, duration and presented text of the text component. Onset is stored by psychopy in the variable  `text_word.tStartRefresh`. The text component has a fixed duration which is logged by the time the routine stops minus the beginning onset refresh time: `VisualStimuliText.stopped - text_word.tStartRefresh`. To have a more detailed description of the event we add the `stimulus_type`, the `stimulus` itself and the `event_type` as custom columns to the tabular `event.tsv` file as a python dictionary under *More* in the builder. To reference a variable of the stimulus file in the dictionary `f"{variable_name}"` is used.

![Properties of the bidsEvent_word_presentation component.](../images/builder_bidsevent_wordpresentation_component.png)

### MouseResponse

![View of the MouseResponse routine in the builder](../images/builder_mouseresponse_routine.png)

This routine consists of two psychopy components, a text component and a mouse component, and two bids task event components.

|Component|Component type|Description|BIDS event
|---|---|---|---
|*text_mouse_response*|[Text component](https://www.psychopy.org/builder/components/text.html)|Presents a fixed text|For the presentation of the text in the *bidsEvent_text_mouse_response* component logs the onset as the presentation of the text, which is stored by psychopy in the variable `text_mouse_response.tStartRefresh`. The routine is terminated by a mouse click, therefore the duration is calculated by the difference of the end of the routine and the start of the text presentation. To have a more detailed description of the event we add the `text_presented` and the `event_type` as a python dictionary in the *More* tab of the builder dialog to the event file.
|*mouse_resp*|[Mouse component](https://www.psychopy.org/builder/components/mouse.html)| Requires a mouse response from the participant| The bids event *bidsEvent_mouse_resp* logs the mouse response. Its onset is defined by the moment a participant responds via a mouse click. This is stored in psychopy as `mouse_resp.time[0]`. Note that psychopy can record multiple mouse clicks time in list, but in this case we are only interested in one -the first- mouse click. As the response is a momentary event it has no duration. To have a more detailed description in the tabular file we add the last position of the mouse (variable: `lastPos`) as `coordinates_mouse_resp` and the `event_type` as python dictionary in the *More* tab of the builder.

### VisualStimuliImage

![View of the VisualStimuliImage routine in the builder](../images/builder_visualstimulusimage_routine.png)

This routine consists of four psychopy components, an image, a text, a keyboard and a code component, and three bids task event components.

|Component | Component type| Description | BIDS event
|---|---|---|---
|*image_presentation*|[Image component](https://www.psychopy.org/builder/components/image.html)| Presents an image to the participant for 2 seconds |  The *bidsEvent_image_presentation* logs the onset of the event as the time the image appears on the screen (variable: `image_presentation.tStartRefresh`). The duration of the image presentation is calculated by the difference of the end of the routine and the refreshing time of the *image_presentation* component. To have a more detailed description of the event in the tabular file we add the `stimulus_type`, the `stimulus` itself and the `event_type` as a python dictionary in the *More* tab of the builder dialog. To reference a variable of the `stimulus file`in the dictionary `f"{variable_name}"` is used.
|*text_image*|[Text component](https://www.psychopy.org/builder/components/text.html)|Presents a fixed text to the participant | The *bidsEvent_text_image* logs the time the text appears (variable: `text_image.tStartRefresh`) as onset and the difference between the end of the routine and the presentation of the text as duration of the event. To have a more detailed description of the event in the tabular file we add the `presented_text` and `event_type` as python dictionary in the *More* tab of the builder dialog.
|*key_resp_image*|[Keyboard component](https://www.psychopy.org/builder/components/keyboard.html)|Requires a keyboard response from the participant|The *bidsEvent_key_resp_image* logs the `responseOnset`(see *code_key_response*) variable as onset of the *key_resp_image* component. As a key press is a momentary event no duration is available. To have a more detailed description of the event in the tabular file we add `key_responded` and `event_type` via a python dictionary in the *More* tab of the builder dialog. To reference a keyboard response in the event file the variable `key_resp_image.keys` is used.
|*code_key_response*|[Code component](https://www.psychopy.org/builder/components/code.html)|This code component calculates the `responseOnset` variable used in the *bidsEvent_key_resp_image* event component by the difference of the start of the image presentation and the reaction time (variable: `code_key_response.rt`). Additionally if no response is given the `responseOnset` is set to `None`. | There is no event for this component.

### SoundStimuli

![View of the SoundStimuli routine in the builder.](../images/builder_soundstimulus_routine.png)

This routine consists of two psychopy components, a sound and a text component, and two bids task event components.

|Component | Component type| Description | BIDS event
|---|---|---|---
|*sound_presentation*|[Sound component](https://www.psychopy.org/builder/components/sound.html)|Presents a sound to the participant|The *bidsEvent_sound_presentation* logs the beginning of the sound (`sound_presentation.tStartRefresh`) as onset and the difference of the end of the routine and the beginning of the sound as duration of the event. To have a more detailed description of the event in the tabular file we add the `stimulus_type`, the `stimulus` itself and the `event_type` via a python dictionary in the *More* tab of the builder dialog. To reference an entry of the *stimulusfile.csv* `f"{variable}"`is used.
|*text_sound*|[Text component](https://www.psychopy.org/builder/components/text.html)|Presents a fixed text to the participant.| The *bidsEvent_text_sound* logs the beginning of the sound as onset (variable: `tStartRefresh`) and the difference between the beginning and the end of the routine as duration of the event. To have a more detailed description of the event in the tabular file we add the `text_presented`and the `event_type` as python dictionary in the *More* tab of the builder dialog.

### MovieStimuli

![View of the MovieStimuli routine in the builder](../images/builder_moviestimulus_routine.png)

The routine consists of a psychopy movie component and a bids task event component.

|Component | Component type| Description | BIDS event
|---|---|---|---
|*movie_presentation*|[Movie component](https://www.psychopy.org/builder/components/movie.html)| Presents a movie to the participant.| The *bidsEvent_movie_presentation* component logs the beginning of the presentation of the movie (`movie_presentation.tStartRefresh`) as onset and the difference of the end and the beginning of the movie as the duration. To have a more detailed description of the event in the tabular file we add the `stimulus_type`, the `stimulus` itself and the `event_type` as python dictionary in the *More* tab of the builder dialog. To reference a variable of the `stimulus file`in the dictionary `f"{variable_name}"` is used.

For demonstration purposes in the example experiment the movie is played without audio (ticked "No audio" under the Playback tab of the movie_species properties). To enable correct sound processing it is necessary to try out different settings within the general settings of psychopy and the sound settings of the component (e.g. different Backend settings). The general audio settings are found by clicking on ![Icon gear](../images/builder_icon_gear.png).

You might get appropriate solutions [here](https://discourse.psychopy.org/t/movie-freezing-at-the-end/29072/12), [here](https://discourse.psychopy.org/t/video-stimuli-not-playing-correctly-a-second-time/19661), [here](https://discourse.psychopy.org/t/videostim-not-working-properly/27690) or [here](https://discourse.psychopy.org/t/unable-to-play-videos/29494)

![The properties of the movie_species component (Playback Tab).](../images/builder_movie_properties.png)

### SliderResponse

![View of the SliderResponse routine in the builder.](../images/builder_sliderresponse_routine.png)

This routine consists of three psychopy components, a text, slider, and code component, and two bids task event components.

|Component | Component type| Description | BIDS event
|---|---|---|---
|*text_slider*|[Text component](https://www.psychopy.org/builder/components/text.html)|Presents fixed text to the participant.|The *bidsEvent_text_slider* logs the beginning of the slider as onset (variable: `text_slider.tStartRefresh`) and the difference between the beginning and the end of the routine as duration of the event. To have a more detailed description of the event in the tabular file we add the `presented_text`and the `event_type` as python dictionary in the *More* tab of the builder dialog.
|*slider_response*|[Slider component](https://www.psychopy.org/builder/components/slider.html)|Presents a slider to the participant and requires response to it. | The *bidsEvent_slider_response* logs the `responseOnset` (see *code_slider_response*) as onset of the slider response. As the response to a slider is a momentary event no duration is available. To have a more detailed description of the event in the tabular file we add the slider positions (variable: `slider_response.markerPos`) as `responded_discrete_value` and the `event_type` as python dictionary in the *More* tab of the builder dialog.
|*code_slider_response*|[Code component](https://www.psychopy.org/builder/components/code.html)|This code component calculates the `responseOnset` variable used in the *bidsEvent_key_resp_image* event component by the difference of the start of the image presentation and the reaction time (variable: `slider_response.rt`). Additionally if no response is given the `responseOnset` is set to `None` and no event is recorded. | There is no event for this component

### GoodbyeScreen

![View of the GoodbyeScreen routine in the builder](../images/builder_goodbye_routine.png)

This routine consists of a psychopy text component and a bids task event component.

|Component | Component type| Description | BIDS event
|---|---|---|---
|*text_goodbye*|[Text component](https://www.psychopy.org/builder/components/text.html)|Presents a fixed text to the participant.| The *bidsEvent_text_goodbye* logs the beginning of the text presentation as onset (variable: `text_goodbye.tStartRefresh`) and the difference between the beginning and the end of the routine as duration of the event. To have a more detailed description of the event in the tabular file we add the `presented_text`and the `event_type` as python dictionary in the *More* tab of the builder dialog.

### BIDS export

![View of the bids_export routine in the builder](../images/builder_bidsexport_routine.png)

This routine ensures that the data collected is exported in the bids format. We specify this as a behavioral experiment, no other data is collected. It's important to include the bids_export routine also into the flow diagram via *Insert Routine*. Note that simply clicking the BIDS export routine in the components tab is not enough, the routine will be created but not added to your experiment.
