# Step 2: Set up task experiment

Different tools are available to build up a task experiment. Depending on the type of data you are collecting a specific tool might be preferable. We provide some recommendations. For some tools we provide active support to obtain bids valid event data. 

For running a purely behavioural task experiment, or a task experiment along an MRI recording, we recommend the use of [PsychoPy](https://www.psychopy.org/). PsychoPy provides graphical user interface for building experimental paradigms, that can be used with little to no learning curve. Additionally, the psychopy-bids plugin provides an easy way to get bids compatible event files directly from your task experiment.

We also offer some [support](https://gitlab.com/ccns/neurocog/neurodataops/anc/software/presentation-to-bids) for the conversion of [Presentation](https://www.neurobs.com/menu_presentation/menu_features/features_overview) log files. 

For MEG experiments, we recommend using [psychtoolbox](http://psychtoolbox.org/), along with the Matlab toolbox written by Thomas Hartmann called [o_ptb](https://o-ptb.readthedocs.io/).

## Implement using PsychoPy builder

To set up your experiment, use the psychopy-template we provide.

!!! Note

    If you are not working in CCNS you cannot directly use the experiment template. However, you can clone the template repository [here](https://gitlab.com/ccns/neurocog/neurodataops/anc/experiments/templates/psychopy-template) and manually copy the files to a new project. 

You can create your experiment project on GitLab in the [*ANC Experiments* group](https://gitlab.com/ccns/neurocog/neurodataops/anc/experiments). Feel free to [contact the ANC](mailto:anc@plus.ac.at) directly for access.

Once you have access:

- Click the blue *Create project* button on the top left of your screen
- Select create a project from template
- Switch the tab to group templates (the third tab)
- Select Psychopy template
- Fill in a descriptive name for your experiment
- Take note of the project slug, this becomes part of the url of your repository. It is automatically generated based on the project name, but your can change it if you like
- Choose your project visibility
- You can add a description if you like

Follow the instructions in the template to install the necessary software. There are additional template issues to help you on your way. The template contains a running experiment that demonstrates some basic features. It covers a range of different event types that you can use in your experiment, which are also logged in bids format using the psychopy-bids plugin. It provides additional instructions on how to run the template and how to change it into your own. Also look at the detailed [description of individual components and routines](./resources/description-example-experiment-builder.md) in the template for additional information if you are using a specific component.

We recommend using GitLab workflow that is described in the [ANC basics](./../../../anc-basics/working-with-data/basic_gitlab_workflow.md).

!!! Note

    If you are using PsychoPy but choose to write the experiment in python you can also get your event files using the [psychopy-bids package](https://psychopy-bids.readthedocs.io/en/stable/coder/). Python based experiments are always executed in a separate virtual environment. You need to provide a `requirements.txt` file to create the environment.

## Implement using Presentation

A limited number of Presentation licenses is available at the PLUS. We offer a basic script that converts Presentation logs to BIDS events. Further processing is necessary to obtain event files such as designed in the first steps. We recommend the use of the event [Remodeler](https://www.hed-resources.org/en/latest/FileRemodelingQuickstart.html).

## Alternative implementation

If you decide on another software to implement your experiment, carefully review the software output to determine if it provides the necessary information. You will have to convert the output to BIDS events yourself before your data is BIDS valid and can be added to the ANC.
