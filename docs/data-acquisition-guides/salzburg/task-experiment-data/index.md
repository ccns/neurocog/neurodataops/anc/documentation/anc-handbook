# Task experiment Salzburg

Information about task events is vital for data analysis. It is the main data for behavioural experiments and essential to analysis of task based MRI/EEG/MEG data. It is important that your task events are recorded and described in a comprehensive manner, so your data can be maximally reusable. We provide general guidelines on planning a comprehensive BIDS event file. We also offer support in implementing a task experiment using [Psychopy](https://www.psychopy.org/). Psychopy provides graphical user interface for building experimental paradigms, that can be used with little to no learning curve. Additionally, the psychopy-bids plugin provides an easy way to get bids compatible event files directly from your task experiment. Additionally, we offer basic conversion scripts for Presentation logs.

- [**Step 1: Plan your events file**](step1_plan_events.md) *Planning your event file ensures you do not miss events and all events have a comprehensive description*
- [**Step 2: Set up task experiment**](step2_implement.md) *We offer some support for implementing your task experiment*
- [**Step 3: Move experiment to test environment**](step3_test_environment.md) *How to take your experiment to the test environment and run it for your study*
- [**Step 4: Annotate your events**](step4_annotate_events.md) *Add descriptions to your event files to make it clear for everyone what happened during the experiment*
