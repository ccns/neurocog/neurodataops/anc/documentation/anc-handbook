# Step 3: Move experiment to test environment

Once you have a running experiment it has to be moved to the test environment, which can be one of the university's behavioural labs or the MRI lab.

Depending on where you are testing this can look somewhat different.

=== "MRI lab"

If you have followed the template instructions you should have a PsychoPy experiment project, which includes, among other files, an `experiment.psyexp` file and a `requirements.txt` file. Before taking your experiment to the scanner you can first test it on a clone of the MRI stimulus computer, directly at the CCNS.

1. At the cloned computer, sign in to the ANC user account

!!! Note "Python version"

     The ANC user account has Python 3.8.10 locally installed (i.e., a per user installation, which can’t be accessed by another user). This is the same as the version we recommend when using the ANC psychopy template experiment. On request a new user account with a possibly different Python version can be created specifically for your work group or your experiment.

2. Clone your project into your user documents directory
     1. Navigate to documents in the explorer
     2. Right click and select *Open git bash*
     3. Type: `clone <experiment-repository>`
3. Create a virtual environment for your experiment
     1. Type *cmd* in search to open the *Command prompt* window
     2. In the command line, navigate to your project directory: `cd <path-to-project>`
     3. Run the following, line by line (replace <project_id\>):
          ```
          python -m venv %USERPROFILE%\environments\<project_id>
          %USERPROFILE%\environments\<project_id>\Scripts\activate.bat
          pip install -r requirements.txt
          ```
     4. Once your packages have installed you can deactivate the environment by running `deactivate` or continue directly with testing your experiment
4. Test your experiment
     1. If you are opening the command line again, first activate your environment running `%USERPROFILE%\environments\<project_id>\Scripts\activate.bat`
     2. Navigate to your experiment project: `cd <path-to-project>`
     3. Open your experiment in PsychoPy: `psychopy experiment.psyexp`
     4. The PsychoPy builder should open with your experiment. From here you can directly run your experiment and make changes when necessary.

!!! Warning "Save changes"
     If you make changes to your `psyexp` file, make sure to save and push your file back to your repository with the made changes!

When your experiment is running properly on the cloned computer, further testing at the scanner is necessary to ensure it works properly with all associated hardware. For this, go to the scanner.

5. Login to the ANC user account at the MRI computer
6. The stimulus computer at the MRI is disconnected from the internet by default. Open network setting from the task bar and disable flight mode
7. Follow the same steps as for the cloned computer: clone your project, and create a virtual environment with your required packages
8. Before testing your experiment, enable flight mode again

!!! Note "Saving changes"

     If further changes are necessary, it is important that you push them to your repository again. For this, you might have to toggle flight mode multiple times, during testing. However, we recommend you turn on flight mode each time you test, because the internet connection might affect the timing of your experiment.
