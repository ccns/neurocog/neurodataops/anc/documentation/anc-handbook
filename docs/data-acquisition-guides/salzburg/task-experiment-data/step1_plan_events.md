# Step 1: Plan your events file

Before setting up the task experiment it is important to plan your events file so that all events that occur during your data recording are captured and logged. This event file can be used later to extract the right information for your analysis. Note that your event file should not exclusively contain the information necessary for analysis. It is important to document the entire experiment. A well-structured event file makes extracting the right information simple. 

We recommend creating a dummy event file. You can start with a simple table. Every event file has an onset and duration column. All other columns are optional in BIDS, but without sufficient additional information data will not be analyzable. However, which columns are necessary depends on the experiment. We recommend starting with an event_type column to classify your events and continue from there.

| onset | duration | event_type |
|---|---|---|
| ... | ... | ... |

## 1. Get a timeline

Draw a timeline of your experiment. Consider, from the beginning of the experiment, what the participants will perceive, what they will see and hear and what actions they may take. This includes instruction screens, fixation crosses, presentation of stimuli and feedback, responses, and any other miscellaneous events that your participants will be aware of.

We use a short section of an experiment as an example. In the following experiment the participants first get an instruction screen. After this they get a fixation cross and a visual stimulus is presented on the screen, which is repeated several times with different visual stimuli. During the presentation of one the fixation crosses the participants press a button.

## 2. Event type

Define the different roles of the events you just identified. Is the event a stimulus, or a response? Depending on what is going on in the experiment, different labels could make sense. If you present different modalities, you might want to distinguish a visual from an auditory stimulus as different event types. But the event type should describe the event on an abstract level. Finer details about the specific event will be added in additional columns.

Write down the identified events in your event table

| onset | duration | event_type |
|---|---|---|
| ... | ... | welcome screen |
| ... | ... | fixation |
| ... | ... | stimulus |
| ... | ... | response |
| ... | ... | feedback |

## 3. Add additional properties for each event type

For every event type you defined, think about the different instances of that event. How do they differ? For instance, in which ways does one stimulus vary from the other, is it a different image, is it associated with a different task, is it presented at a different position on the screen, does it belong to a different experimental condition? Does it belong to a different trial? Each of these properties should be described in a new column.

!!! Note "Conditions and trial numbers"

    Some information might apply to more than one event, for example, multiple stimuli might be part of one trial. The entire trial together might make up one condition. In such a case, each stimuli should still be marked as a separate event. However, to simplify processing later on, it is useful to add a trial_nr column, and ensure each event in a single trial has the same trial_nr. Similar, if multiple events make up a condition, the condition can be marked on each event. This also applies to responses and their accuracy. It is useful to indicate on a stimulus event, whether the task associated with this stimulus was completed correctly.

You can add rows to the table to reflect the different values that will be represented in a column but it is not necessary to be exhaustive, the dummy file is there purely for reference.

| onset | duration | event_type | position | image |
|---|---|---|---|---|
| ... | ... | stimulus | left | someimage.jpg |
| ... | ... | stimulus | right | someotherimage.jpg |

If a column is not relevant to an event type, this column will remain empty and get a n/a for that row. You can add as many columns as necessary. Events that are always the same, or that only occur once, such as the welcome screen, do not need to be described in additional columns. Considering all event types your table might look something like this:

| onset | duration | event_type | position | image | button | accuracy | feedback_type | condition |
|---|---|---|---|---|---|---|---|---|
| ... | ... | welcome screen | n/a | n/a | n/a | n/a | n/a | n/a |
| ... | ... | fixation | n/a | n/a | n/a | n/a | n/a | n/a |
| ... | ... | stimulus | n/a | n/a | n/a | n/a | n/a | happy |
| ... | ... | stimulus | left | someimage.jpg | n/a | n/a | n/a | sad |
| ... | ... | stimulus | right | someotherimage.jpg | n/a | n/a | n/a | n/a |
| ... | ... | response | n/a | n/a | a | 0 | n/a | happy |
| ... | ... | response | n/a | n/a | b | 1 | n/a | sad |
| ... | ... | feedback | n/a | n/a | n/a | n/a | congruent | happy |
| ... | ... | feedback | n/a | n/a | n/a | n/a | incongruent | sad |

Keep the dummy event file. When you set up the experiment with your chosen experiment software, check back with your example to ensure your output file matches the planned event file.

!!! Warning "Post processing might be necessary"

    Sometimes it is not possible to directly add relevant information during data acquisition. For example, there might be timing concerns when it comes to calculating correct responses and directly writing them to response and stimulus rows. In this case, postprocessing is necessary. We recommend using the [remodeler tool](https://www.hed-resources.org/en/latest/FileRemodelingQuickstart.html) to easily process event files.
    