# ANC Basics

Welcome to ANC basics. [Austrian NeuroCloud](https://anc.plus.ac.at/) is powered by [GitLab](https://about.gitlab.com/). GitLab is a leading, fully integrated, software development platform with [Git version control system](https://git-scm.com/) at its core. Although GitLab was originally designed for software development, it also fits other applications, like data management and data integration.

With an ANC GitLab account, you get access to data management features of the Austrian NeuroCloud. Some of them are:

- Create and maintain data projects,
- Automate data acquisition, BIDS conversion, and ingestion,
- Automate data validation and consistency,
- Manage access to your data,
- Automate processing pipelines,
- Curate your data.

There is a learning curve for using GitLab, but once you get it hang of it there are a lot of advantages. The most difficult part is to learn certain workflows. For general information you can check out the [GitLab user documentation](https://docs.gitlab.com/ee/user/index.html). We point to it whenever possible. Instructions specific to the ANC or ANC recommended workflows are explained in the handbook. We recommend to read through all of the basics is this is your first time using our repository.
