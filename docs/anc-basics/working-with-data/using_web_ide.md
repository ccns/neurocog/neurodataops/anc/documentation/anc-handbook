# Using the Web IDE

Gitlab provides a simple way of editing regular files without making a local copy of the repository.

!!! Warning "Don't change large files in the WebIDE"

    Due to a bug in GitLab it is currently not possible to make changes to large files in the WebIDE. Such files must always be handled locally. Changing these files in the WebIDE will corrupt their content. Large files are marked as LFS in the repository. They include nifty, image and audio files, among others.

## Open the Web IDE

!!! Note "There are two ways to open the Web IDE"

     - Starting from a merge request
     - Starting from the repository

### Starting from a merge request

Navigate to the merge request you want to work on and click on the *Code* button on the right-handed side and select *Open in Web IDE*.

![open ide](./images/open_webide.png)

It will additionally open all changes that have been done so far on the merge request.

### Starting from the repository

Click on the *Edit* button and select *Web IDE*.

![open ide repository](./images/web_ide_repository.png)

!!! warning Disclaimer

    Always make sure your working on the right branch!

If no branch was selected beforehand, the Web IDE opens on the main branch, which is displayed in the bottom left corner. By clicking on the branch name, a bar at the top of the screen will open, allowing you to select the branch you want to work on.

![web ide main](./images/web_ide_main.png){width=75%}

## Working on files

The Web IDE functions similarly to Visual Studio Code, but without extensions.

![web ide](./images/web_ide.png)

Various types of text files, such as `.md`, `.tsv`, and `.json`, can now be easily edited.

Additionally, new files can be created or files uploaded by right-clicking on the folder structure panel.

## Commit changes

After modifying a file, a notification appears in the source control. Access the source control to commit (1) the changes by clicking on the symbol. 
{ .annotate }

1. By committing a change, you save them in the branch your working on. Each commit should include a commit message discribing what was implemented.  

![change message](./images/change_message.png){width=45%} &rarr; ![commit](./images/commit.png){width=45%}

It's advisable to make small commits that contain only a single change or very few changes. This practice enables better tracking and provides a clear overview of all modifications.

Once the changes were committed, simply close the Web IDE.
    
