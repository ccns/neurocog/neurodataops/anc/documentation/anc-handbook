# Working locally

To work on your data locally you need a local copy of the repository, which you get by [cloning the repository](./download_data.md).

To ensure that all local changes are properly synchronized with the GitLab repository and that your local repository is up-to-date with the remote repository, adherence to established GitLab workflows is essential.

## Git status

The command `git status` offers a comprehensive overview at various points throughout your workflow.

!!! Note "It provides information on:"

    - The branch you're currently working on
    - If you are ahead or behind the remote repository
    - Changes you have made
    - Any untracked files

In the commandline navigate to the repository and enter:

`git status`

Also see the [git documentation](https://git-scm.com/docs/git-status).

## Change branch

As outlined in our [basic GitLab workflow](./basic_gitlab_workflow.md), it is crucial to ensure that work is done on the correct branch.

To view the available remote branches, use the following command:

`git branch -r`

This will list all remote branches. To switch to a specific branch, use the following command:

`git checkout yourbranch`

![git checkout branch](./images/change_branch.png)

!!! warning Disclaimer

    If you do **not** have `--skip-smudge` as default setting for git lfs (see guide on [cloning repository](./download_data.md)), checking out a new branch will pull all LFS files. This can be avoided by using `GIT_LFS_SKIP_SMUDGE=1` in front of every git command.

## Pull changes

Before working on the repository it is important to make sure you are working on the latest version of the data. Therefor, pull the current version of your data first, use the following command:

`git pull`

This will make sure that your local repository is up to date with its online counterpart. If not, changes you make locally might conflict with changes that have been made online.

## Make changes

Once on the appropriate branch, changes can be made to files or directories on the local machine.

Ensure that each change is committed. After applying modifications, use the `git status` command to confirm that changes are listed.

![git status](./images/git_status.png)

Add the changes with:

`git add changedfile`

Then commit the change:

`git commit -m "your commit message"`

![add and commit](./images/add_and_commit.png)

Finally, push commits to the remote repository:

`git push`

This requires your username and [access token](download_data.md#access-token).

![git push](./images/git_push.png)
