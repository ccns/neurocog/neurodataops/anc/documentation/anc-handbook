# Working with the data

The ANC operates on GitLab, to effectively manage and interact with your data set repository within ANC, it is crucial to have a solid understanding of basic GitLab operations.

This guide provides an overview of the essential workflows and tools necessary for efficient data repository management. It is designed to help users navigate the GitLab environment, covering key practices for organizing, editing, and maintaining data files. By familiarizing yourself with these fundamental operations, you will be better equipped to handle various aspects of your data repository, ensuring a smooth and productive workflow.

- [Basic GitLab workflow](./basic_gitlab_workflow.md): Understand the core concepts and steps involved in using GitLab, including creating issues and merge requests, and commiting changes.
- [Using the Web IDE](./using_web_ide.md): Learn how to edit, add, and commit files directly within the GitLab interface using the Web IDE.
- [Cloning the repository](./download_data.md): Detailed instructions on how to clone repositories to your local machine, including handling LFS files.
- [Working locally](./working_locally.md): Make changes on your local machine, committing those changes, and synchronizing with the remote repository.
