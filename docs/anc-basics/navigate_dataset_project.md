# Navigate your dataset project

When you open an ANC dataset project it contains several files. Some of these are part of the BIDS dataset and need to be filled in, and other files take care of ANC operations and should be left alone.

In the following we provide an overview of the files contained in your repository. Next we describe the template issues that are part of every project and which should be the starting point for working in your project. To help you with your dataset, each dataset project is assigned to a data steward. They are there to ensure your data is organized for maximum FAIRness, and to answer any questions related to adding your data, formatting your data, or solving the template issues.

## File overview

Within a newly created dataset project your can find the following files:

![dataset files](./images/dataset_files.png)

!!! Note "Phenotype only"

    If you are in a phenotype only project there is also a phenotype directory with template files for your phenotypic data. These are explained further in out [formatting guides](../data-formatting-guides/questionnaire_data.md). Here we only explain the files that are common among all dataset templates.

Note the icons left to the filenames. They indicate the file type. Some files are necessary for ANC operations. You don't need to worry about these files but we provide a short description for background:

- `.bidsignore`: ensures bids validation can go through (artefacts generated during bids validation should be ignored by the validator)
- `.gitattributes`: configuration file for git, ensures that large data files are treated appropriately by git version control
- `.gitignore`: more configuration for git, ensures your derivatives are not added to the repository
- `.gitlab-ci.yml`: GitLab continuous integration file, ensures bids validation is run on your data and your dataset website is generated automatically

Other files need to be filled in by you. This can be done using the template issues described in the next section.

- `CITATION.cff`: contains information about how this dataset should be cited, your dataset website is generated based on this file
- `README.md`: a general description of your dataset, this readme is provided on the dataset website
- `dataset_description.json`: dataset metadata that includes information about funding and ethical approval, information about BIDS version is maintained by the data stewards
- `participants.json`: describes the contents of the `participants.tsv`
- `participants.tsv`: contains demographic information about study participants

## Template issues

Your ANC dataset project comes with a set of template issues. You can find them under *Plan* > *Issues* in the left side panel. These issues are *to do's* that describe which files need to be updated by you and ensure your dataset contains all necessary information. Some of this information is used by the ANC to generate your dataset website. DOIs provided by the ANC link to this website, which provides an overview of the your dataset without giving direct access to the dataset files.

We recommend you start with resolving some of the template issues. A general description of how to resolve an issue (and how to create issues for yourself) can be found under [Working with data](./working-with-data/basic_gitlab_workflow.md).

## Dataset Data Steward

Each dataset is assigned to an ANC data steward. You can find them in the issue called *Dataset Status*. Whoever is assigned to the issue (see Assigned in the right panel) is there to assist you in putting your data on the ANC. You can leave any questions you may have in the Dataset Status issue. You can also mention the data steward in any of the template issue, or issues that you open yourself, in case you have a question or require assistance (type `@<data-steward-name>`, their user name should appear in the drop down menu).