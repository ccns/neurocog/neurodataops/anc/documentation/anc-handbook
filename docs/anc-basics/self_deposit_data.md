# Self-deposit guide 

This guide assumes that you have a dataset which conforms to the current [BIDS specification](https://bids-specification.readthedocs.io/en/stable/).

**Read this guide until the end before you start depositing your data.**

Additionally to BIDS, your dataset should satisfy the ANC's [Data Format Requirements](../terms/data_format_requirements.md). Please verify that. If you have any problems with satisfying the requirements, have a look at our [Data Formatting Guides](../data-formatting-guides/index.md).

The ANC is built with GitLab which uses Git for version control. Although our guides explain the commands and functionalities of these systems, you should have basic skills in Git and shell usage, for example bash.

## Create an Austrian NeuroCloud account

To deposit your data you need an [ANC account](./get_an_account.md).

!!! info "You accept the ANC Terms of Use"

    By creating the ANC account and signing in into the system, you accept the [ANC Terms](../terms/index.md). In particular, as depositor you accept the [Transfer and License Agreement](../terms/transfer_and_license_agreement.md) and agree to license your dataset with the [ANC License](../terms/anc_license.md).

## Create a Dataset Project

Every [dataset project belongs to a research unit group](../terms/code_of_conduct.md#groups-structure) in [BIDS Datasets](https://data.anc.plus.ac.at/bids-datasets). If you don't find your research unit group, please ask to create the group for you by sending an email to `anc@plus.ac.at` including: your user name, the full name of the group, its abbreviated name used in URLs, and a description.

Once your research unit group is available, [create a new dataset project](./create_dataset_project.md).

## Understand the dataset project files

A new dataset project comes with several [predefined files](./navigate_dataset_project.md#file-overview). Please make yourself familiar with them.

## Data curation and stewardship

Curation of your dataset, including editing metadata as well as adding participant data, usually follows the [Basic GitLab workflow](./working-with-data/basic_gitlab_workflow.md). Issues (Plan > Issues) define the work that should be done for your dataset. Some issues are already there for you to start.

The [ANC data stewards](../terms/data_stewardship_policy.md) assist you in the data deposition and review changes to your data. Moreover, they learn your data in the process, help you resolve any issues, and offer metadata improvements.

The "Dataset Status" issue, labeled <span class="label dataset-status has-scope">Dataset status<span class="scope">Initialized</span></span>, specifies the data steward assigned to your dataset (Asignee) and serves as a communication channel with your data steward. Leave a comment there if you need any help.

## ANC Task issues and metadata

A fresh dataset project comes with several predefined ANC Task issues, labeled <span class="label anc-task has-scope">ANC Task<span class="scope">*</span></span>, which guide you through adding metadata to your dataset. The descriptions of the issues provide you with the necessary instructions and links to further documentation. Always read the description before you start working.

Resolve the ANC Task issues following our [workflows for working with data](./working-with-data/index.md).

## Add participant data files

Every dataset is different. To tailor the ANC functionalities to your dataset, the data steward needs to understand your data. Therefore, we divide the process of depositing participant data in two.

Adding participant data follows [Basic GitLab workflow](./working-with-data/basic_gitlab_workflow.md) with [Step 3](./working-with-data/basic_gitlab_workflow.md#step-3-work-on-the-issue) executed [on the machine where your data resides](./working-with-data/working_locally.md).

### Pilot participant

Pick one participant with the most complete data. In this step you add all data relevant **only** to this participant, including:

- participant data files (excluding phenotype-only datasets): the entire `sub-` directory of the chosen participant,
- participant metadata: one row in `participants.tsv` file for the chosen participant and the missing column descriptions in the `participants.json` file (for more information see your project's issue with <span class="label anc-task has-scope">ANC Task<span class="scope">9</span></span> label).

[Create a new issue](./working-with-data/basic_gitlab_workflow.md#step-1-create-an-issue) with a descriptive title, for example "Pilot participant", and briefly describe the expected participant data: sessions, data types, number of scans, participant information, etc.

!!! note "Mandatory session directories"

    The ANC requires every participant directory to have [at least one session](../terms/data_format_requirements.md#session-directories).

[Create a merge request](./working-with-data/basic_gitlab_workflow.md#step-2-create-merge-request) and follow the [local workflow](./working-with-data/working_locally.md) to add participant files.

!!! warning "Add imaging data files using git commands locally"

    Use the [local workflow](./working-with-data/working_locally.md) to deposit participant data. Imaging files are stored using Git LFS, which is not available in GitLab's WebIDE.

    This is a [known GitLab issue](https://gitlab.com/gitlab-org/gitlab/-/issues/217828).

After pushing all data files, [view the changes in the merge request](./working-with-data/basic_gitlab_workflow.md#step-4-view-changes) and [request a review and data merge](./working-with-data/basic_gitlab_workflow.md#step-5-request-merge) from your data steward. Please give your data steward some time to respond.

The data steward will assist you with the merge request, ask for potential clarifications, recommend necessary changes, or ask for more pilot participants if necessary.

After completed review, the data of your pilot participant will be merged :tada:

### Remaining participants

Continue with this step only after the data of your pilot participant has been merged in.

Follow the steps of the [Pilot participant](#pilot-participant). This time you can add the data of all the remaining participants.

!!! warning "Do not add too many participants at once"

    The ANC can handle large amount of data. However, to keep the system responsive and simplify the interaction with the data stewards, consider adding the participants in batches.

## Questionnaire data

We recommend to add questionnaire data after all participant data has been deposited.

Follow the steps in you project's issue "Add Questionnaire Data" labeled <span class="label anc-task has-scope">ANC Task<span class="scope">45</span></span>.
