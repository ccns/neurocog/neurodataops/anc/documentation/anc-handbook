# Request access to a restricted dataset

!!! info "Temporary process"

    To accelerate the development of the Austrian NeuroCloud (ANC), we have implemented this simplified process. A more robust and sustainable solution is currently under development.

By default, datasets in the ANC are restricted. However, each dataset has a dedicated website to ensure findability, such as [this example](https://bids-datasets.data-pages.anc.plus.ac.at/neurocog/soccer/). Once you have identified the dataset's website, you can proceed to request access.

## Access restrictions

Access to restricted datasets is limited to registered ANC users. If you do not have an ANC account, one will be created for you during the access request process.

Due to legal regulations, datasets are only available for research or educational purposes and may only be shared with scientific personnel. Sharing with industry partners is not permitted.

## Requesting access

To request access to a dataset available via its website under `[dataset website]`, please send an email to `anc@plus.ac.at` using the following template.

[**Click this link**](mailto:anc@plus.ac.at?subject=Access%20request%20to%20%5Bdataset%20website%5D&body=Full%20name%3A%0D%0A%0D%0AAffiliation%3A%0D%0A%0D%0AWork%20email%3A%0D%0A%0D%0AANC%20user%20name%20(if%20applicable)%3A%0D%0A%0D%0ADataset%20website%3A%20%5Bdataset%20website%5D%0D%0A%0D%0AReason%20for%20the%20request%3A%0D%0A) to open the template in your default email client[^1].

**Email subject:** Access request to `[dataset website]`

**Email content:**
```
Full name:

Affiliation:

Work email:

ANC user name (if applicable):

Dataset website: [dataset website]

Reason for the request:
```

[^1]: Email link generated with [https://mailto.vercel.app/](https://mailto.vercel.app/).
