# ANC organization

ANC stores datasets as GitLab [projects](https://docs.gitlab.com/ee/user/project/organize_work_with_projects.html) organized in [groups](https://docs.gitlab.com/ee/user/group/).

## Dataset projects and groups

ANC focuses solely on datastes in [BIDS](https://bids-specification.readthedocs.io/en/stable/) format. Each BIDS dataset is stored in one GitLab **dataset project** in a **research unit group**, which is direct subgroup of [BIDS Datasets](https://data.anc.plus.ac.at) group.

In general, one dataset project should correspond to a single study. Deviations from this rule, for example, splitting acquired data types in multiple projects, should be discussed on a case-by-case basis and documented.

## User permissions and roles

GitLab implements a fine grained [user permission scheme](https://docs.gitlab.com/ee/user/permissions.html). Users are added to groups and projects as members with specific roles. The roles limit user actions.

## Project visibility

By default, each [dataset project is private](https://docs.gitlab.com/ee/user/public_access.html). This means, that only users, which were explicitly added as members of the project, can view it and contribute to it. The goal of the ANC is to increase the number of publicly available datasets. This, however, requires more legal investigation.
