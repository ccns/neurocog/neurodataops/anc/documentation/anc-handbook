# Open a dataset project

!!! warning "Always open a dataset project from an ANC template"

    Any dataset repository on the ANC should be opened from an ANC project template, regardless of whether you are uploading an existing BIDS dataset, or are acquiring data with one of ANC's automatic conversion pipelines. The template contains basic files that are required for any valid BIDS dataset as well as files that take care of essential ANC operations, such as BIDS validation and the generation of a dataset specific website. Additionally, the template comes with predefined "issues" or to-do's that help you prepare your dataset.

Open a dataset project from the correct template. All datasets containing MEG, EEG, MRI, or task behavioral data, should be opened from the *bids-basic* template. If your dataset only contains questionnaire data (phenotypic data in BIDS), use the *BIDS Phenotypic-only* template.

To open a dataset project you require and ANC account and you should be assigned to at least one working group, where you will create the dataset project.

<div class="annotate" markdown>

1. Navigate to your working group in under [BIDS Datasets](https://data.anc.plus.ac.at), and click new project in the top right corner.
2. Select *Create from template*.
3. There are three tabs over the listed templates, select *Instance* to see all ANC templates and select the correct template for your data type.
4. Now you create your dataset project. Check whether your project is in the correct namespace under *Project URL*. This should be in `BIDS datasets/<working group label>`. For example, `bids-datasets/neurocog` for the Neurocognition Lab.
5. Provide a descriptive name for your dataset.
6. Replace the project slug(1) if necessary.

!!! warning

     If you are using any of the ANC automated pipelines for injecting your data, the project slug has to match the project identifier of the dataset that is provided in filenames (see [site-specific instructions](../data-acquisition-guides/salzburg/mri-data/part2/step1_subject_registration.md) for more information).

7. (Optional) Add a description for your dataset project.
8. Set the visibility of your dataset project to *Private* (See [ANC Code of Conduct](../terms/code_of_conduct.md#dataset-projects)).
9. Click *Create project* to finish creating your dataset project.

</div>

1. When you provide the name for your dataset, GitLab automatically fills in your project slug based on the name. You can use any name for your dataset project, we recommend you use a descriptive name and avoid abbreviations. You are free to use capital letters and spaces. **However, the project slug will become the part of part of the URL or address to your dataset**. It does not need to match the dataset name, it cannot contain spaces and should not contain capital letters.
