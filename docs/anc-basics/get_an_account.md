# Get an account

!!! info "You accept the ANC Terms of Use"

    By creating the ANC account and signing in into the system, you accept the [ANC Terms](../terms/index.md).
    
    By accessing any dataset, you agree to the terms of the attached license.
    
    As depositor you accept the [Transfer and License Agreement](../terms/transfer_and_license_agreement.md) and agree to license your dataset with the [ANC License](../terms/anc_license.md).

!!! info "Temporary process"

    To accelerate the development of the Austrian NeuroCloud (ANC), we have implemented this simplified process. A more robust and sustainable solution is currently under development.

## Account restrictions

At the moment, during the prototyping phase, depositing in the ANC is strictly limited. Accounts will be created on demand. Eligible are members of [Centre for Cognitive Neuroscience](https://ccns.plus.ac.at/), [MRI Lab](https://psychologie.uni-graz.at/en/mri-lab/), and their collaborators.

Visit the [ANC website](https://anc.plus.ac.at/) to learn more about the project, or [contact us](mailto:anc@plus.ac.at) for more information.

## Requesting access

To request the ANC account, please send an email to `anc@plus.ac.at` using the following template.

As depositor, you can request creation of a [research unit group](../terms/code_of_conduct.md#groups-structure), to which your datasets should belong. Browse through [current groups](https://data.anc.plus.ac.at/bids-datasets) to verify if your group already exists.

[**Click this link**](mailto:anc@plus.ac.at?subject=Account%20request&body=Full%20name%3A%0D%0A%0D%0AAffiliation%3A%0D%0A%0D%0AWork%20email%3A%0D%0A%0D%0AResearch%20unit%20group%20full%20name%20(for%20depositor%20and%20new%20group)%3A%0D%0A%0D%0AResearch%20unit%20group%20URL%20abbreviation%20(for%20depositor%20and%20new%20group)%3A%0D%0A%0D%0AReason%20for%20the%20request%3A) to open the template in your default email client[^1].

**Email subject:** Account request

**Email content:**
```
Full name:

Affiliation:

Work email:

Research unit group full name (for depositor and new group):

Research unit group URL abbreviation (for depositor and new group):

Reason for the request:
```

[^1]: Email link generated with [https://mailto.vercel.app/](https://mailto.vercel.app/).

