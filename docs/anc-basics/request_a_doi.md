# Request a DOI

Before you can get a DOI for your dataset, it has to meet the [ANC format requirements](./../terms/data_format_requirements.md). Also see our [Persistent Identifier Policy](./../terms/pid_policy.md) for more information.

Once you meet the requirements you can request a DOI by opening an issue on your dataset project. A data steward will be notified and handle your request.

1. Go to your project and open a new issue
2. Title your issue *DOI request*
3. Under *Description* click *Choose a template* and select  *request_doi*

![request doi issue](./images/request_doi.JPG)

4. The template will appear in markdown. For easier editing you can select *Switch to rich text editing* under the issue description.

![switch_editing](./images/switch_editing.PNG)

5. Read the instructions, do not remove or edit them
6. Complete the checklist
7. If you need an exception, type an explanation under **Additional comments**

We try to process your request for a DOI as fast as possible. 