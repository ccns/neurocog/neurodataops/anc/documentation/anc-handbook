# FAQ

**What is a *data repository?***

A dataset repository in the ANC is a GitLab repository which contains all data acquired in a study, along with comprehensive metadata. We accept all data structured according to Brain Imaging Data Structure BIDS. 

Data is continuously validated. For certain data types we provide additional quality checks.

**How can I create my own data repository?**

Please follow our QuickStart Guides.
