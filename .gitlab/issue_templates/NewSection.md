# New section

This issue proposes a new section, a collection of pages, for the handbook. You can also use it to propose reorganizing specific pages. You should fill in at least the *Content summary*. You can leave the other sections empty if you do not know what to fill in. All sections should be filled in before starting work on the issue. Please also see our [contributing guidelines](https://gitlab.com/ccns/neurocog/neurodataops/anc/documentation/anc-handbook/-/blob/main/CONTRIBUTING.md?ref_type=heads) for instructions on how to use this template or create another type of issue.

## Section summary

*Please summarize the section that should be added in the handbook. Give examples of pages that would fit here. Summarize the index page. If you have already proposed pages for this section, link the issues.*

## Section type

*What type of section are you proposing? Possible types are: Top level section, divide existing pages into sections, or add a new section in a series of section e.g. a datatype under the acquisition guides*

## Additional information

*Depending on what type of section you add, provide some additional context*

*Please indicate whether you were able to complete the sections to make it easier for the issue to be picked up. If you proposed something but are not certain you can leave the box unchecked.*

*If you are adding a new top-level section, explain why your content does not fit existing top-levels. What other documentation can be provided in this section (it should be sufficiently plausible that this section will fulfil a general purpose)*

*To add a new level to the hierarchy, consider if other pages should be moved. Describe which pages should be moved, and where.*

## Checklist

- [ ] Section summary
- [ ] Section type
- [ ] Additonal information

/label ~"New section"

/label ~"Documentation"