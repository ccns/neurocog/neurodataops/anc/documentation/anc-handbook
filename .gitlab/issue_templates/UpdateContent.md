# Update content

This issue suggests an update to an existing page in the handbook, either to correct or clarify something that is already described in the handbook. You should fill in at least the *Content summary*. You can leave the other sections empty if you do not know what to fill in. All sections should be filled in before starting work on the issue. Please also see our [contributing guidelines](https://gitlab.com/ccns/neurocog/neurodataops/anc/documentation/anc-handbook/-/blob/main/CONTRIBUTING.md?ref_type=heads) for instructions on how to use this template or create another type of issue.

## Content place

*Where should this content be added in the handbook? Please describe the path from a top level section to the new content.*

## Update summary

*Please describe the correction of clarification you want to make.*

## Checklist

*Please indicate whether you were able to complete the sections to make it easier for the issue to be picked up.*

- [ ] Content place
- [ ] Update summary

/label ~"Update content"

/label ~"Documentation" 
