# New page

This issue proposes a new page for the handbook. The page should fit into an existing section. You should fill in at least the *Content summary*. You can leave the other sections empty if you do not know what to fill in. All sections should be filled in before starting work on the issue. Please also see our [contributing guidelines](https://gitlab.com/ccns/neurocog/neurodataops/anc/documentation/anc-handbook/-/blob/main/CONTRIBUTING.md?ref_type=heads) for instructions on how to use this template or create another type of issue.

## Content summary

*Please summarize the content that should be added in the handbook.*

## Content type

*What type of content should be added to the handbook? Possible content types are: step-by-step-guide, documentation-of-policies, documentation-of-background.*

## Content place

*Where should this content be added in the handbook? Please describe the path from a top level section to the new content.*

## Checklist

*Please indicate whether you were able to complete the sections to make it easier for the issue to be picked up. If you proposed something but are not certain you can leave the box unchecked.*

- [ ] Content summary
- [ ] Content type
- [ ] Content place

/label ~"New page"

/label ~"Documentation"
