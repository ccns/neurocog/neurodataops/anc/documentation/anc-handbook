# Interface update

This issue proposes an update to the interface or functionality of the handbook. This can include changes to header functionalities.

## Update summary

*Describe the proposed update.*

## Checklist

*Indicate whether you were able to fill in all necessary information*

- [ ] Update summary

/label ~"Update interface" 

/label ~"Documentation"
